<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PdbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pdb')->insert([
            'visitor' =>  1581594500,
            'student' => json_encode(
                [
                    'name' => 'DNS Progress',
                    'gender' => 'LAKI-LAKI',
                    'nisn' => 9965416816,
                    'nik' => 3574012008970001,
                    'birth_place' => 'Probolinggo',
                    'birth_date' => 20,
                    'birth_month' => 8,
                    'birth_year' => 1997,
                    'religion' => 'Islam',
                    'disability' => 'Tidak',
                    'address' => 'Jalan Nangka Dusun Krajan RT 2 RW 3 Kelurahan Pohsangit Kidul Kecamatan Kademangan Kota Probolinggo',
                    'village' => 'Dusun Krajan',
                    'rt' => 2,
                    'rw' => 3,
                    'village_office' => 'Pohsangit Kidul',
                    'postal_code' => 67226,
                    'sub_district' => 'Kademangan',
                    'city' => 'Probolinggo',
                    'province' => 'Jawa Timur',
                    'vehicle' => 'Jalan Kaki',
                    'live' => 'Bersama Orang Tua',
                    'house_phone' => '0335890112',
                    'phone' => '089643385968',
                    'email' => 'dnsprogress@gmail.com',
                    'kps' => 'YA',
                    'kps_number' => '890113344111',
                    'nationality' => 'WNI',
                    'pp' => 'TIDAK',
                    'bank' => 'BRI',
                    'bank_account' => '65100199022891',

                    'father_name' => 'Father Dummy',
                    'father_birth_year' => 1980,
                    'father_disability' => 'Tidak',
                    'father_job' => 'Wirausaha',
                    'father_education' => 'SMA / Sederajat',
                    'father_salary' => 'Rp 2.000.000 - Rp 4.999.999',
                    'mother_name' => 'Mother Dummy',
                    'mother_birth_year' => 1985,
                    'mother_disability' => 'Tidak',
                    'mother_job' => 'Tidak Bekerja',
                    'mother_education' => 'S1',
                    'mother_salary' => '< Rp 500.000',
                    'guardian_name' => '',
                    'guardian_birth_year' => '',
                    'guardian_disability' => '',
                    'guardian_job' => '',
                    'guardian_education' => '',
                    'guardian_salary' => '',

                    'height' => 175,
                    'weight' => 65,
                    'distance' => 3,
                    'distance_time' => 10,
                    'brother' => 2,

                    'achievements' => [
                        [
                            'kind' => 'Lomba Renang',
                            'level' => 'Kota',
                            'name' => 'Juara 1 Lomba Renang Kota Probolinggo',
                            'year' => 2020,
                            'organizer' => 'Kota Probolinggo'
                        ],
                        [
                            'kind' => 'Lomba Lari',
                            'level' => 'Provinsi',
                            'name' => 'Juara 2 Lomba Lari Kota Probolinggo',
                            'year' => 2019,
                            'organizer' => 'Provinsi Jawa Timur'
                        ]
                    ],

                    'scholarships' => [
                        [
                            'kind' => 'Beasiswa Kecerdasan Siswa',
                            'organizer' => 'SMA Negeri 1 Kota Probolinggo',
                            'year_start' => 2019,
                            'year_finish' => 2020
                        ],
                        [
                            'kind' => 'Bidikmisi',
                            'organizer' => 'Pemerintah Indonesia',
                            'year_start' => 2020,
                            'year_finish' => 2024
                        ]
                    ]
                ]
            ),
            'status' => 0
        ]);
    }
}
