@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova</title>
<meta name="title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Pendaftaran Peserta Didik Baru</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.pdb.step-1') }}">PDB</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Contact Us -->
<section id="contact" class="contact section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h5 class="mb-3">Terima Kasih, Pendaftaran Anda Berhasil!</h5>
                    <p class="mb-4">Untuk mengkonfirmasi pendaftaran kepada Admin Sekolah Alam Cordova, klik tombol dibawah ini</p>
                    <div class="form-cordova">
                        <div class="form-group">
                            <button type="button" class="btn" id="confirm-button" data-name="{{ $student->name }}">KONFIRMASI PENDAFTARAN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-head">
            <div class="row">
                <div class="col-lg-12">

                </div>

            </div>
        </div>
    </div>
</section>
<!--/ End Contact Us -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#confirm-button').click(function(event) {
            event.preventDefault();
            let name = $(this).data('name');
            let message = $('[name=message]').val();
            let templateMessage =   window.encodeURIComponent (
                                'Hai, Admin Sekolah Cordova \n\n' +
                                'Nama Saya : ' + name + '\n\n' +
                                'Dengan ini saya mengkonfirmasi bahwa saya telah melakukan pendaftaran siswa baru Sekolah Alam Cordova \n\n\n' +
                                'Terima Kasih.'
                            );
            let url = 'https://web.whatsapp.com/send?phone=6285258795244&text=' + templateMessage;
            waSend(url, 'Salam Cordova', 1050, 550);
        })

        function waSend(url, title, width, height) {
            let left = (screen.width - width) / 2;
            let top = (screen.height - height) / 4;
            let myWindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
        }
    })
</script>
@endsection