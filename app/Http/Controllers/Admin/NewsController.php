<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNews;
use App\Http\Requests\UpdateNews;
use App\News;
use App\Services\CloudinaryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.news.index', [
            'newses' => News::orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $request)
    {
        $request->validated();

        $news = new News;
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->image = CloudinaryService::upload($request->file('image'));
        $news->pageview = 0;
        $news->slug = Str::slug($news->title);

        if($news->save()) {
            return redirect()->route('news.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.news.edit', [
            'news' => News::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNews $request, $id)
    {
        $request->validated();

        $news = News::findOrFail($id);
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->slug = Str::slug($news->title);

        if ($request->file('image')) {
            CloudinaryService::remove($news->image);
            $news->image = CloudinaryService::upload($request->file('image'));
        }

        if($news->save()) {
            return redirect()->route('news.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        if($news->delete()) {
            CloudinaryService::remove($news->image);
            return redirect()->route('news.index');
        }
    }
}
