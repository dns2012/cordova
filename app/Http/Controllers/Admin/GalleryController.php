<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGallery;
use App\Http\Requests\UpdateGallery;
use App\Services\CloudinaryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.gallery.index', [
            'galleries' => Gallery::orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    public function indexStage($stage)
    {
        if (! in_array($stage, ['TK', 'SD', 'SMP'])) {
            abort(404);
        }

        session([
            'gallery' => $stage
        ]);

        return view('admin.gallery.index', [
            'galleries' => Gallery::where('stage', $stage)->orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGallery $request)
    {
        $request->validated();

        $gallery = new Gallery;
        $gallery->stage = session('gallery');
        $gallery->title = $request->input('title');
        $gallery->image = CloudinaryService::upload($request->file('image'));
        $gallery->slug = Str::slug($gallery->title);

        if($gallery->save()) {
            return redirect()->route('gallery.stage', session('gallery'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.gallery.edit', [
            'gallery' => Gallery::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGallery $request, $id)
    {
        $request->validated();

        $gallery = Gallery::findOrFail($id);
        $gallery->title = $request->input('title');
        $gallery->slug = Str::slug($gallery->title);

        if ($request->file('image')) {
            CloudinaryService::remove($gallery->image);
            $gallery->image = CloudinaryService::upload($request->file('image'));
        }

        if($gallery->save()) {
            return redirect()->route('gallery.stage', session('gallery'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        if($gallery->delete()) {
            CloudinaryService::remove($gallery->image);
            return redirect()->route('gallery.stage', session('gallery'));
        }
    }
}
