<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProgram;
use App\Http\Requests\UpdateProgram;
use App\Program;
use App\Services\CloudinaryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.program.index', [
            'programs' => Program::orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProgram $request)
    {
        $request->validated();

        $program = new Program;
        $program->title = $request->input('title');
        $program->description = $request->input('description');
        $program->image = CloudinaryService::upload($request->file('image'));
        $program->slug = Str::slug($program->title);

        if($program->save()) {
            return redirect()->route('program.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.program.edit', [
            'program' => Program::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProgram $request, $id)
    {
        $request->validated();

        $program = Program::findOrFail($id);
        $program->title = $request->input('title');
        $program->description = $request->input('description');
        $program->slug = Str::slug($program->title);

        if ($request->file('image')) {
            CloudinaryService::remove($program->image);
            $program->image = CloudinaryService::upload($request->file('image'));
        }

        if($program->save()) {
            return redirect()->route('program.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::findOrFail($id);
        if($program->delete()) {
            CloudinaryService::remove($program->image);
            return redirect()->route('program.index');
        }
    }
}
