@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Teacher</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('teacher.index') }}">Teacher</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Index
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height">
                @foreach ($teachers as $teacher)
                <div class="col-lg-3 col-md-3">
                    <div class="card">
                        <div class="card-body block-teacher__card-body">
                            <h4 class="card-title block-teacher__card-body-title">{{ $teacher->name }}</h4>
                        </div>
                        <img class="block-teacher__card-image" src="{{ $teacher->avatar }}" alt="Card image">
                        <div class="card-body text-center block-teacher__card-body-quote">
                            {{ Str::words($teacher->motto, 15) }}
                        </div>
                        <div class="card-footer block-teacher__card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                            <div class="block-teacher__card-footer-grid">
                                <div class="block-teacher__card-footer-grid-item">
                                    <button class="btn btn-info full-width" title="Edit" onclick="window.location='{{ route('teacher.edit', $teacher->id) }}'">
                                        <i class="ft-edit"></i> Edit
                                    </button>
                                </div>
                                <div class="block-teacher__card-footer-grid-item">
                                    <form action="{{ route('teacher.destroy', $teacher->id) }}" method="POST" onsubmit="return confirm('Are you sure?')">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger full-width" title="Delete">
                                            <i class="ft-trash"></i> Delete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row block-nav justify-content-center">
                <div class="col-lg-12 col-7 block-nav__box">
                   {{ $teachers->links() }}
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sticky no-padding" title="Add New" onclick="window.location='{{ route('teacher.create') }}'">
        <i class="ft-plus la-3x"></i>
    </button>
</div>
@endsection