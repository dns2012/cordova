<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                'title' => 'Pendidikan Agama Sejak Dini Menjadi Kunci Generasi Berakhlak',
                'description' => 'Misi kami adalah mencetak pribadi berakhlak mulia dan unggul serta berilmu yang bermanfaat untuk banyak masyarakat.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/banner-1.jpg',
                'slug' => 'pendidikan-agama-sejak-dini-menjadi-kunci-generasi-berakhlak'
            ],
            [
                'title' => 'Belajar Langsung dan Mengenal Lebih Dekat Dengan Lingkungan Sekitar',
                'description' => 'Kami didik para siswa untuk lebih mengenal dengan lingkungan sekitar melalui cocok tanam, perdagangan dan wirausaha.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/banner-2.jpg',
                'slug' => 'kami-didik-para-siswa-untuk-lebih-mengenal-dengan-lingkungan-sekitar-melalui-cocok-tanam-perdagangan-dan-wirausaha'
            ],
            [
                'title' => 'Bukan Hanya Berbasis Agama, IPTEK Juga Menjadi Pelengkap Siswa',
                'description' => 'Tujuan bukan hanya mencetak siswa berakhlak mulia tapi juga seimbang dengan IPTEK yang terus mengikuti perkembangan zaman.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/banner-3.jpg',
                'slug' => 'bukan-hanya-berbasis-agama-iptek-juga-menjadi-pelengkap-siswa'
            ]
        ]);
    }
}
