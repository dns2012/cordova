<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galleries')->insert([
            [
                'stage' => 'TK',
                'title' => 'Gallery 1',
                'image' => 'https://sekolahalamindonesia.org/wp-content/uploads/2019/12/IMG-20131218-WA0004-360x270.jpg',
                'slug' => 'gallery-1'
            ],
            [
                'stage' => 'SD',
                'title' => 'Gallery 2',
                'image' => 'https://sekolahalamindonesia.org/wp-content/uploads/2019/12/IMG-20150221-WA0000-360x203.jpg',
                'slug' => 'gallery-2'
            ],
            [
                'stage' => 'SMP',
                'title' => 'Gallery 3',
                'image' => 'https://sekolahalamindonesia.org/wp-content/uploads/2019/12/young-leadership-camp1-360x270.jpeg',
                'slug' => 'gallery-3'
            ],
        ]);
    }
}
