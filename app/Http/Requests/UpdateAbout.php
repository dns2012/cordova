<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateAbout extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prologue' => ['required', 'string'],
            'vision' => ['required', 'string'],
            'mission' => ['required', 'string'],
            'excellent_program' => ['numeric'],
            'active_student' => ['numeric'],
            'professional_teacher' => ['numeric'],
            'award_received' => ['numeric'],
        ];
    }
}
