<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Pdb;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index($id, $stage)
    {
        if ($stage != 'all') {
            $like_stage = '%'.$stage.'%';
            $pdbs = Pdb::where('status', $id)
                        ->where('student', 'LIKE', $like_stage)
                        ->orderBy('created_at', 'DESC')
                        ->paginate(8);
        } else {
            $pdbs = Pdb::where('status', $id)->orderBy('created_at', 'DESC')->paginate(8);
        }
        return view('admin.pdb.index', [
            'pdbs' => $pdbs,
            'stage' => $stage
        ]);
    }

    public function detail($id)
    {
        $model = Pdb::findOrFail($id);
        return view('admin.pdb.detail', [
            'student' => json_decode($model->student),
            'model' => $model
        ]);
    }

    public function print($id)
    {
        $model = Pdb::findOrFail($id);
        return view('admin.pdb.print', [
            'student' => json_decode($model->student),
            'model' => $model
        ]);
    }

    public function process(Request $request, $id)
    {
        $model = Pdb::findOrFail($id);
        $model->status = 2;
        $model->save();

        return redirect()->route('pdb.index', [2, 'all']);
    }
}
