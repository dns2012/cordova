<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            [
                'title' => 'Inilah cara kami belajar',
                'description' => 'SL Exhibition, bukan hanya sebuah ekshibisi, tapi ini adalah SELEBRASI program yang',
                'image' => 'https://salamcordova.sch.id/learnedu/images/event-1.jpg',
                'date' => date('Y-m-d'),
                'time' => date('H:i'),
                'slug' => 'inilah-cara-kami-belajar'
            ],
            [
                'title' => 'SL Exhibition',
                'description' => 'It’s not just an exhibition. It’s a celebration from our school program in Sekolah Lanjutan',
                'image' => 'https://salamcordova.sch.id/learnedu/images/event-2.jpg',
                'date' => date('Y-m-d'),
                'time' => date('H:i'),
                'slug' => 'sl-exhibition'
            ],
            [
                'title' => 'I-Journey',
                'description' => 'Individual journey atau biasa disingkat i-journey merupakan program yang dirancang untuk siswa',
                'image' => 'https://salamcordova.sch.id/learnedu/images/event-3.jpg',
                'date' => date('Y-m-d'),
                'time' => date('H:i'),
                'slug' => 'i-journey'
            ]
        ]);
    }
}
