<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert([
            'name' => 'Anies Baswedan',
            'position' => 'Guru Agama',
            'motto' => 'Orang-orang baik tumbang bukan hanya karena banyaknya orang jahat, tetapi karena orang-orang baik lainnya diam dan mendiamkan.',
            'avatar' => 'https://indeksnews.com/wp-content/uploads/2019/11/5-1-2.png',
            'media' => json_encode([
                'facebook' => 'https://www.facebook.com/aniesbaswedan/',
                'twitter' => 'https://twitter.com/aniesbaswedan?lang=en',
                'linkedin' => 'https://www.linkedin.com/in/aniesbaswedan/?originalSubdomain=id',
                'instagram' => 'https://www.instagram.com/aniesbaswedan/?hl=en'
            ]),
            'slug' => 'anies-baswedan'
        ]);
    }
}
