@for ($u=0;$u<=3;$u++) <div class="form-group">
    <div class="row">
        <div class="col-lg-12 mb-2">
            <label>Jenis Beasiswa</label>
            <input type="text" name="scholarships[{{ $u }}][kind]" value="@if(! empty($student->scholarships[$u]->kind)){{ $student->scholarships[$u]->kind }}@endif">
            @error('scholarships.'.$u.'.kind')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-12 mb-2">
            <label>Penyelenggara</label>
            <input type="text" name="scholarships[{{ $u }}][organizer]" value="@if(! empty($student->scholarships[$u]->organizer)){{ $student->scholarships[$u]->organizer }}@endif">
            @error('scholarships.'.$u.'.organizer')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Tahun Mulai</label>
            <select name="scholarships[{{ $u }}][year_start]">
                <option value="">--</option>
                @for ($i=1970;$i<=2030;$i++) <option value="{{ $i }}" @if (! empty($student->scholarships[$u]->year_start) && $student->scholarships[$u]->year_start==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('scholarships.'.$u.'.year_start')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Tahun Selesai</label>
            <select name="scholarships[{{ $u }}][year_finish]">
                <option value="">--</option>
                @for ($i=1970;$i<=2030;$i++) <option value="{{ $i }}" @if (! empty($student->scholarships[$u]->year_finish) && $student->scholarships[$u]->year_finish==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('scholarships.'.$u.'.year_finish')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    </div>
    @endfor