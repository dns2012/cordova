<?php

namespace App\Http\Controllers\Frontend;

use App\Event;
use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        return view('frontend.news.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'newses' => News::orderBy('id', 'DESC')->paginate(9),
        ]);
    }

    public function detail($slug = null)
    {
        return view('frontend.news.detail', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'news' => News::where('slug', $slug)->firstOrFail() 
        ]);
    }
}
