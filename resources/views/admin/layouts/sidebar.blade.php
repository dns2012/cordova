@section('sidebar')
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="{{ asset('cameleon') }}/images/backgrounds/02.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('dashboard') }}"><img class="brand-logo" alt="admin logo" src="{{ asset('cameleon') }}/images/ico/icon.png" />
                    <h3 class="brand-text">Salam Cordova</h3>
                </a>
            </li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'dashboard') === false ? '' : 'active' }}">
                <a href="{{ route('dashboard') }}">
                    <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'admin') === false ? '' : 'active' }}">
                <a href="{{ route('admin.index') }}">
                    <i class="ft-users"></i><span class="menu-title" data-i18n="">Admin</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'banner') === false ? '' : 'active' }}">
                <a href="{{ route('banner.index') }}">
                    <i class="ft-monitor"></i><span class="menu-title" data-i18n="">Banner</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'about') === false ? '' : 'active' }}">
                <a href="{{ route('about.edit', 1) }}">
                    <i class="ft-github"></i><span class="menu-title" data-i18n="">Profil</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'program') === false ? '' : 'active' }}">
                <a href="{{ route('program.index') }}">
                    <i class="ft-layers"></i><span class="menu-title" data-i18n="">Program</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'teacher') === false ? '' : 'active' }}">
                <a href="{{ route('teacher.index') }}">
                    <i class="ft-user-check"></i><span class="menu-title" data-i18n="">Pengajar</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'event') === false ? '' : 'active' }}">
                <a href="{{ route('event.index') }}">
                    <i class="ft-calendar"></i><span class="menu-title" data-i18n="">Acara</span>
                </a>
            </li>
            <li class="nav-item {{ strpos(Route::currentRouteName(), 'news') === false ? '' : 'active' }}">
                <a href="{{ route('news.index') }}">
                    <i class="ft-share-2"></i><span class="menu-title" data-i18n="">Berita</span>
                </a>
            </li>
            <li class="nav-item has-sub {{ strpos(Route::currentRouteName(), 'gallery') === false ? '' : 'open' }}">
                <a href="#">
                    <i class="ft-image"></i><span class="menu-title" data-i18n="">Galeri</span>
                </a>
                <ul class="menu-content">
                    <li class="@if (session('gallery') == 'TK') active @endif">
                        <a class="menu-item" href="{{ route('gallery.stage', 'TK') }}">TK</a>
                    </li>
                    <li class="@if (session('gallery') == 'SD') active @endif">
                        <a class="menu-item" href="{{ route('gallery.stage', 'SD') }}">SD</a>
                    </li>
                    <li class="@if (session('gallery') == 'SMP') active @endif">
                        <a class="menu-item" href="{{ route('gallery.stage', 'SMP') }}">SMP</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-sub {{ strpos(Route::currentRouteName(), 'pdb') === false ? '' : 'open' }}">
                <a href="#">
                    <i class="ft-file"></i><span class="menu-title" data-i18n="">PPDB</span>
                </a>
                <ul class="menu-content">
                    <li class="@if (Route::currentRouteName() == 'pdb.index' && Route::current()->parameter('status') == 1) active @endif">
                        <a class="menu-item" href="{{ route('pdb.index', [1, 'all']) }}">Baru</a>
                    </li>
                    <li class="@if (Route::currentRouteName() == 'pdb.index' && Route::current()->parameter('status') == 2) active @endif">
                        <a class="menu-item" href="{{ route('pdb.index', [2, 'all']) }}">Proses</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="navigation-background"></div>
</div>
@endsection