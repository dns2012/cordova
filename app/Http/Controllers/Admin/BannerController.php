<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBanner;
use App\Http\Requests\UpdateBanner;
use App\Services\CloudinaryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.banner.index', [
            'banners' => Banner::orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBanner $request)
    {
        $request->validated();

        $banner = new Banner;
        $banner->title = $request->input('title');
        $banner->description = $request->input('description');
        $banner->image = CloudinaryService::upload($request->file('image'));
        $banner->slug = Str::slug($banner->title);

        if($banner->save()) {
            return redirect()->route('banner.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.banner.edit', [
            'banner' => Banner::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBanner $request, $id)
    {
        $request->validated();

        $banner = Banner::findOrFail($id);
        $banner->title = $request->input('title');
        $banner->description = $request->input('description');
        $banner->slug = Str::slug($banner->title);

        if ($request->file('image')) {
            CloudinaryService::remove($banner->image);
            $banner->image = CloudinaryService::upload($request->file('image')->path());
        }

        if($banner->save()) {
            return redirect()->route('banner.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        if($banner->delete()) {
            CloudinaryService::remove($banner->image);
            return redirect()->route('banner.index');
        }
    }
}