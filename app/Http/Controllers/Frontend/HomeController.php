<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Banner;
use App\Event;
use App\Http\Controllers\Controller;
use App\News;
use App\Program;
use App\Teacher;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {

        return view('frontend.home.index', [
            'banners' => Banner::orderBy('id', 'DESC')->limit(5)->get(),
            'about' => About::findOrFail(1),
            'programs' => Program::orderBy('id', 'DESC')->get(),
            'teachers' => Teacher::orderBy('id', 'DESC')->get(),
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'news' => News::orderBy('id', 'DESC')->limit(3)->get()
        ]);
    }
}
