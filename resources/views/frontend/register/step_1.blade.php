<div class="form-group">
    <label>Nama Lengkap</label>
    <input type="text" name="name" value="@if(! empty($student->name)){{ $student->name }}@endif" required>
    @error('name')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Jenis Kelamin</label>
    <select name="gender" required>
        <option value="">--</option>
        <option value="LAKI-LAKI" @if (! empty($student->gender) && $student->gender=='LAKI-LAKI') selected @endif>Laki-laki</option>
        <option value="PEREMPUAN" @if (! empty($student->gender) && $student->gender=='PEREMPUAN') selected @endif>Perempuan</option>
    </select>
    @error('gender')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Jenjang Pendaftaran</label>
    <select name="stage" required>
        <option value="">--</option>
        <option value="TK" @if (! empty($student->stage) && $student->stage=='TK') selected @endif>TK</option>
        <option value="SD" @if (! empty($student->stage) && $student->stage=='SD') selected @endif>SD</option>
        <option value="SMP" @if (! empty($student->stage) && $student->stage=='SMP') selected @endif>SMP</option>
    </select>
    @error('stage')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Nomor Induk Siswa Nasional</label>
    <input type="text" name="nisn" value="@if(! empty($student->nisn)){{ $student->nisn }}@endif" required>
    @error('nisn')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Nomor Induk Kependudukan</label>
    <input type="text" name="nik" value="@if(! empty($student->nik)){{ $student->nik }}@endif" required>
    @error('nik')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Tempat dan Tanggal Lahir</label>
    <div class="row">
        <div class="col-lg-5">
            <input type="text" name="birth_place" value="@if(! empty($student->birth_place)){{ $student->birth_place }}@endif" required>
            @error('birth_place')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-2">
            <select name="birth_date" required>
                <option value="">--</option>
                @for ($i=1;$i<=31;$i++) <option value="{{ $i }}" @if (! empty($student->birth_date) && $student->birth_date==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('birth_date')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-2">
            <select name="birth_month" required>
                <option value="">--</option>
                @for ($i=1;$i<=12;$i++) <option value="{{ $i }}" @if (! empty($student->birth_month) && $student->birth_month==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('birth_month')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-3">
            <select name="birth_year" required>
                <option value="">--</option>
                @for ($i=1997;$i<=2020;$i++) <option value="{{ $i }}" @if (! empty($student->birth_year) && $student->birth_year==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('birth_year')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="form-group">
    <label>Agama</label>
    <input type="text" name="religion" value="@if(! empty($student->religion)){{ $student->religion }}@endif" required>
    @error('religion')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Berkebutuhan Khusus</label>
    <input type="text" name="disability" value="@if(! empty($student->disability)){{ $student->disability }}@endif" required>
    @error('disability')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Alamat Tempat Tinggal</label>
    <input type="text" name="address" value="@if(! empty($student->address)){{ $student->address }}@endif" required>
    @error('address')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-8 mb-2">
            <label>Dusun</label>
            <input type="text" name="village" value="@if(! empty($student->village)){{ $student->village }}@endif">
            @error('village')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-2 mb-2">
            <label>RT</label>
            <input type="text" name="rt" value="@if(! empty($student->rt)){{ $student->rt }}@endif" required>
            @error('rt')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-2 mb-2">
            <label>RW</label>
            <input type="text" name="rw" value="@if(! empty($student->rw)){{ $student->rw }}@endif" required>
            @error('rw')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-8 mb-2">
            <label>Kelurahan</label>
            <input type="text" name="village_office" value="@if(! empty($student->village_office)){{ $student->village_office }}@endif">
            @error('village_office')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-4 mb-2">
            <label>Kode Pos</label>
            <input type="text" name="postal_code" value="@if(! empty($student->postal_code)){{ $student->postal_code }}@endif" required>
            @error('postal_code')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Kecamatan</label>
            <input type="text" name="sub_district" value="@if(! empty($student->sub_district)){{ $student->sub_district }}@endif" required>
            @error('sub_district')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Kabupaten / Kota</label>
            <input type="text" name="city" value="@if(! empty($student->city)){{ $student->city }}@endif" required>
            @error('city')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Provinsi</label>
            <input type="text" name="province" value="@if(! empty($student->province)){{ $student->province }}@endif" required>
            @error('province')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="form-group">
    <label>Kendaraan Sekolah</label>
    <select name="vehicle" required>
        <option value="">--</option>
        @foreach ($vehicle as $vehicle)
        <option value="{{ $vehicle }}" @if (! empty($student->vehicle) && $student->vehicle==$vehicle) selected @endif>{{ $vehicle }}</option>
        @endforeach
    </select>
    @error('vehicle')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Jenis Tinggal</label>
    <select name="live" required>
        <option value="">--</option>
        @foreach ($live as $live)
        <option value="{{ $live }}" @if (! empty($student->live) && $student->live==$live) selected @endif>{{ $live }}</option>
        @endforeach
    </select>
    @error('live')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Telepon Rumah</label>
    <input type="text" name="house_phone" value="@if(! empty($student->house_phone)){{ $student->house_phone }}@endif">
    @error('house_phone')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>No HP</label>
    <input type="text" name="phone" value="@if(! empty($student->phone)){{ $student->phone }}@endif" required>
    @error('phone')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Email</label>
    <input type="email" name="email" value="@if(! empty($student->email)){{ $student->email }}@endif">
    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Penerima KPS?</label>
    <select name="kps" required>
        <option value="">--</option>
        <option value="YA" @if (! empty($student->kps) && $student->kps=='YA') selected @endif>YA</option>
        <option value="TIDAK" @if (! empty($student->kps) && $student->kps=='TIDAK') selected @endif>TIDAK</option>
    </select>
    @error('kps')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>No KPS <i>(jika menerima KPS)</i></label>
    <input type="text" name="kps_number" value="@if(! empty($student->kps_number)){{ $student->kps_number }}@endif">
    @error('kps_number')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Kewarganegaraan</label>
    <select name="nationality" required>
        <option value="">--</option>
        <option value="WNI" @if (! empty($student->nationality) && $student->nationality=='WNI') selected @endif>WNI</option>
        <option value="WNA" @if (! empty($student->nationality) && $student->nationality=='WNA') selected @endif>WNA</option>
    </select>
    @error('nationality')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Usulan Layak PP</label>
    <select name="pp" required>
        <option value="">--</option>
        <option value="YA" @if (! empty($student->pp) && $student->pp=='YA') selected @endif>YA</option>
        <option value="TIDAK" @if (! empty($student->pp) && $student->pp=='TIDAK') selected @endif>TIDAK</option>
    </select>
    @error('pp')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Bank</label>
    <input type="text" name="bank" value="@if(! empty($student->bank)){{ $student->bank }}@endif">
    @error('bank')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>No Rekening Bank</label>
    <input type="text" name="bank_account" value="@if(! empty($student->bank_account)){{ $student->bank_account }}@endif">
    @error('bank_account')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>