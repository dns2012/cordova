<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('frontend.contact.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
        ]);
    }
}
