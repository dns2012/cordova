<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->insert([
            'prologue' => 'Sekolah Alam terinspirasi dari ide Bapak H.M. Soleh seorang warga Probolinggo yang sangat peduli dengan pendidikan dan memiliki 2 bidang tanah dengan kondisi alam yang unik. Berbekal keikhlasan mencari Ridlo Allah SWT dibuatlah Yayasan Ashabul Muslikh Al Khusyu’ sebagai dasar mendirikan sekolah. belum adanya SMP Swasta yang layak untuk menjadi sekolah unggulan di Kec. Wonoasih Kota Probolinggo, menjadikan sangat perlunya keberadaan Sekolah Alam ini
            Kemudian menggandeng tim work Perintis yang terdiri dari 3 orang praktisi pendidikan, maka dibuatlah sekolah dengan memanfaatkan alam yang ada sebagai tempat belajar utama. Alam berarti: pengalaman, semesta alam, makhluk dan segala sesuatu yang diciptakan Allah SWT.Sekolah ini dibuat sesuai tujuan pendidikan Islam mencetak Khalifatullah fil ardhi yang berakhlaq mulia.',
            'vision' => 'Terwujudnya lembaga pendidikan Islam yang unggul, berkarakter dan berbudaya lingkungan',
            'mission' => '<ol>
            <li>Islam sebagai rujukan utama sekolah</li>
            <li>Membangun pendidikan yang integral, professional dan berMUTU</li>
            <li>Mengutamakan perintah Allah di bidang Keimanan, Ubudiyah, Akhlaq</li>
            <li>Mengembangkan potensi siswa baik akademik dan non akademik</li>
            <li>Mengembangkan lingkungan alam yang Islami, ilmiah, ilahiyah</li>
            <li>Menjalin hubungan strategis dengan lembaga terkait</li>
            </ol>',
            'point' => json_encode([
                'excellent_program' => 23,
                'active_student' => 120,
                'professional_teacher' => 13,
                'award_received' => 5
            ])
        ]);
    }
}
