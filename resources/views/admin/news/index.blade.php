@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">News</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('news.index') }}">News</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Index
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height">
                @foreach ($newses as $news)
                <div class="col-lg-3 col-md-3">
                    <div class="card">
                        <img class="block-news__card-image" src="{{ $news->image }}" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title block-news__card-body-title">{{ $news->title }}</h4>
                            <h6 class="card-subtitle text-muted block-news__card-body-subtitle">{{ strip_tags(Str::words($news->description, 15)) }}</h6>
                        </div>
                        <div class="card-footer block-news__card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                            <div class="block-news__card-footer-grid">
                                <div class="block-news__card-footer-grid-item">
                                    <button class="btn btn-info full-width" title="Edit" onclick="window.location='{{ route('news.edit', $news->id) }}'">
                                        <i class="ft-edit"></i> Edit
                                    </button>
                                </div>
                                <div class="block-news__card-footer-grid-item">
                                    <form action="{{ route('news.destroy', $news->id) }}" method="POST" onsubmit="return confirm('Are you sure?')">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger full-width" title="Delete">
                                            <i class="ft-trash"></i> Delete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row block-nav justify-content-center">
                <div class="col-lg-12 col-7 block-nav__box">
                   {{ $newses->links() }}
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sticky no-padding" title="Add New" onclick="window.location='{{ route('news.create') }}'">
        <i class="ft-plus la-3x"></i>
    </button>
</div>
@endsection