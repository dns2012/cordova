<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@salamcordova.sch.id',
            'password' => Hash::make('salamcordova'),
            'role' => 1,
            'avatar' => 'https://pngimage.net/wp-content/uploads/2018/05/default-user-profile-image-png-7.png'
        ]);
    }
}
