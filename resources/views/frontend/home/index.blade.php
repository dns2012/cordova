@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Sekolah Alam Cordova</title>
<meta name="title" content="Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Slider Area -->
<section class="home-slider">
	<div class="slider-active">
		@foreach ($banners as $banner)
		<!-- Single Slider -->
		<div class="single-slider overlay" style="background-image:url('{{ $banner->image }}')">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-12">
						<div class="slider-text">
							<h1>{{ $banner->title }}</h1>
							<p>{{ $banner->description }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Single Slider -->
		@endforeach
	</div>
</section>
<!--/ End Slider Area -->

<!-- Features -->
<section class="our-features section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-10 wow zoomIn">
				<div class="section-title">
					<h2>Tentang <span>Kami</span></h2>
					{!! $about->prologue !!}
					<a href="{{ route('front.about') }}">read more..</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Features -->

<!-- Fun Facts -->
<div class="fun-facts" data-stellar-background-ratio="0.5">
	@php $point = json_decode($about->point); @endphp
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.4s">
				<!-- Single Fact -->
				<div class="single-fact">
					<i class="fa fa-book"></i>
					<div class="number"><span class="counter">{{ $point->excellent_program }}</span>+</div>
					<p>Program Unggulan</p>
				</div>
				<!--/ End Single Fact -->
			</div>
			<div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.6s">
				<!-- Single Fact -->
				<div class="single-fact">
					<i class="fa fa-graduation-cap"></i>
					<div class="number"><span class="counter">{{ $point->active_student }}</span>+</div>
					<p>Siswa Aktif</p>
				</div>
				<!--/ End Single Fact -->
			</div>
			<div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.8s">
				<!-- Single Fact -->
				<div class="single-fact">
					<i class="fa fa-users"></i>
					<div class="number"><span class="counter">{{ $point->professional_teacher }}</span>+</div>
					<p>Pengajar Profesional</p>
				</div>
				<!--/ End Single Fact -->
			</div>
			<div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="1s">
				<!-- Single Fact -->
				<div class="single-fact">
					<i class="fa fa-trophy"></i>
					<div class="number"><span class="counter">{{ $point->award_received }}</span>+</div>
					<p>Penghargaan Diterima</p>
				</div>
				<!--/ End Single Fact -->
			</div>
		</div>
	</div>
</div>
<!--/ End Fun Facts -->

<!-- Courses -->
<section class="courses section-bg section">
	<div class="container">
		<div class="row">
			<div class="col-12 wow zoomIn">
				<div class="section-title mb-3">
					<h2>Program <span>Kami</span></h2>
					<p>Program BBA(Belajar Bersama Alam) Adalah proses KBM dengan kondisi alam yang meliputi kondisi lingkungan, tumbuhan, hewan, dan kehidpuan manusia sebagai media belajarnya.</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			@foreach ($programs as $program)
			<div class="col-lg-4 mb-4">
				<!-- Single Course -->
				<div class="single-course">
					<div class="course-head overlay">
						<img src="{{ $program->image }}" alt="#">
						<a href="{{ route('front.program.detail', $program->slug) }}" class="btn"><i class="fa fa-link"></i></a>
					</div>
					<div class="single-content">
						<h4 style="height: 38px"><a href="{{ route('front.program.detail', $program->slug) }}">{{ $program->title }}</a></h4>
						<div style="height: 75px">
							<p>{{ strip_tags(Str::words($program->description, 10)) }}</p>
						</div>
					</div>
				</div>
				<!--/ End Single Course -->
			</div>
			@endforeach
		</div>
	</div>
</section>
<!--/ End Courses -->

<!-- Call To Action -->
<section class="cta" data-stellar-background-ratio="0.5">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 offset-lg-6 col-12">
				<div class="cta-inner overlay">
					<div class="text-content">
						<h2>Kami Fokus Membangun Peserta Didik Muslim Yang Cerdas, Ramah dan Tangguh</h2>
						<p>Ini merupakan landasan dasar bagi kami untuk mewujudkan visi misi generasi yang berakhlak mulia dan berwawasan luas.</p>
						<div class="button">
							<a class="btn primary wow fadeInUp" href="{{ route('front.pdb.step-1') }}">Daftar Sekarang</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Call To Action -->

<!-- Team -->
<section class="team section" style="padding-bottom: 0px !important">
	<div class="container">
		<div class="row">
			<div class="col-12 wow zoomIn">
				<div class="section-title mb-3">
					<h2>Pengajar <span>Profesional</span></h2>
					<p>Upaya kami mewujudkan peserta didik unggul tak luput dari peran tiap komponen pengajar. </p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="teacher-slider">
					@foreach ($teachers as $teacher)
					<div class="single-team">
						<div>
							<img src="{{ $teacher->avatar }}" style="width: 100%; height: 100%; object-fit: cover;" alt="#">
						</div>
						<div class="team-hover">
							<h4>{{ $teacher->name }}<span>{{ $teacher->position }}</span></h4>
							<p>{{ $teacher->motto }}</p>
							@php $media = json_decode($teacher->media) @endphp
							<ul class="social">
								<li><a href="@if(! empty($media->facebook)){{ $media->facebook }}@else{{ 'javascript:void(0)' }}@endif" @if(! empty($media->facebook)) target="blank" @endif><i class="fa fa-facebook"></i></a></li>
								<li><a href="@if(! empty($media->twitter)){{ $media->twitter }}@else{{ 'javascript:void(0)' }}@endif" @if(! empty($media->twitter)) target="blank" @endif><i class="fa fa-twitter"></i></a></li>
								<li><a href="@if(! empty($media->linkedin)){{ $media->linkedin }}@else{{ 'javascript:void(0)' }}@endif" @if(! empty($media->linkedin)) target="blank" @endif><i class="fa fa-linkedin"></i></a></li>
								<li><a href="@if(! empty($media->instagram)){{ $media->instagram }}@else{{ 'javascript:void(0)' }}@endif" @if(! empty($media->instagram)) target="blank" @endif><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Team -->

<!-- Events -->
<section class="events section">
	<div class="container">
		<div class="row">
			<div class="col-12 wow zoomIn">
				<div class="section-title mb-3">
					<h2>Acara / Event <span>Kami</span></h2>
					<p>Bukan hanya KBM kami juga mengadakan acara dan kegiatan sosial bagi siswa dan masyarakat sekitar</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="event-slider">
					@foreach ($events as $event)
					<!-- Single Event -->
					<div class="single-event">
						<div class="head overlay">
							<img src="{{ $event->image }}" alt="#">
							<a href="{{ $event->image }}" data-fancybox="photo" class="btn"><i class="fa fa-search"></i></a>
						</div>
						<div class="event-content">
							<div class="meta">
								<span><i class="fa fa-calendar"></i>{{ date('d F Y', strtotime($event->date)) }}</span>
								<span><i class="fa fa-clock-o"></i>{{ date('H.i A', strtotime($event->time)) }}</span>
							</div>
							<h4 style="height: 35px"><a href="{{ route('front.event.detail', $event->slug) }}">{{ $event->title }}</a></h4>
							<p>{{ strip_tags(Str::words($event->description, 10)) }}</p>
							<div class="button">
								<a href="{{ route('front.event.detail', $event->slug) }}" class="btn">Ikuti Acara</a>
							</div>
						</div>
					</div>
					<!--/ End Single Event -->
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Events -->

<!-- Enroll -->
<section class="enroll overlay section" data-stellar-background-ratio="0.5">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<div class="row">
					<div class="col-lg-6 col-12 wow fadeInUp" data-wow-delay="0.4s">
						<!-- Single Enroll -->
						<div class="enroll-form">
							<div class="form-title">
								<h2>Yuk Gabung Sekarang</h2>
								<p>Jangan lewatkan kesempatan belajar secara langsung praktek kehidupan sehari-hari !</p>
							</div>
							<!-- Form -->
							<form class="form" action="#">
								<div class="form-group">
									<label>Nama Lengkap</label>
									<input name="name" type="text" placeholder="Andri Alamsyah">
								</div>
								<div class="form-group">
									<label>No. HP (WA Orang Tua)</label>
									<input name="number" type="email" placeholder="+62">
								</div>
								<div class="form-group">
									<label>Pesan</label>
									<textarea name="message" placeholder="Saya ingin mendaftar..."></textarea>
								</div>
								<div class="form-group button">
									<button type="button" class="btn" onclick="window.location='{{ route('front.pdb.step-1') }}'">Gabung Sekarang</button>
								</div>
							</form>
							<!--/ End Form -->
						</div>
						<!-- Single Enroll -->
					</div>
					<div class="col-lg-6 col-12 wow fadeInUp" data-wow-delay="0.6s">
						<div class="enroll-right">
							<div class="section-title">
								<h2>Mari Gabung Bersama Kami</h2>
								<p>Mari bergabung dan menjadi bagian keluarga besar kami. Memerdekan belajar, menyatu dengan alam, tentunya menjadikan generasi Khalifatullah fil ardhi yang berakhlaq mulia.</p>
							</div>
						</div>
						<!-- Skill Main -->
						<div class="skill-main">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 wow zoomIn" data-wow-delay="0.8s">
									<!-- Single Skill -->
									<div class="single-skill">
										<div class="circle" data-value="0.7" data-size="130">
											<strong>{{ $point->active_student }}+</strong>
										</div>
										<h4>Siswa</h4>
									</div>
									<!--/ End Single Skill -->
								</div>
								<div class="col-lg-4 col-md-4 col-12 wow zoomIn" data-wow-delay="1s">
									<!-- Single Skill -->
									<div class="single-skill">
										<div class="circle" data-value="0.9" data-size="130">
											<strong>{{ $point->excellent_program }}+</strong>
										</div>
										<h4>Program</h4>
									</div>
									<!--/ End Single Skill -->
								</div>
								<div class="col-lg-4 col-md-4 col-12 wow zoomIn" data-wow-delay="1.2s">
									<!-- Single Skill -->
									<div class="single-skill">
										<div class="circle" data-value="0.8" data-size="130">
											<strong>{{ $point->professional_teacher }}+</strong>
										</div>
										<h4>Pengajar</h4>
									</div>
									<!--/ End Single Skill -->
								</div>
							</div>
						</div>
						<!--/ End Skill Main -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Enroll -->

<!-- Blogs -->
<section class="blog section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title mb-3">
					<h2>Berita <span>Terbaru</span></h2>
					<p>Setiap kegiatan atau wacana penting kami dokumentasikan berupa tulisan singkat maupun artikel.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="blog-slider">
					@foreach ($news as $news)
					<!-- Single Blog -->
					<div class="single-blog">
						<div class="blog-head overlay">
							<div class="date">
								<h4>{{ date('d', strtotime($news->created_at)) }}<span>{{ date('M', strtotime($news->created_at)) }}</span></h4>
							</div>
							<img src="{{ $news->image }}" alt="#">
						</div>
						<div class="blog-content">
							<h4 class="blog-title" style="height: 60px"><a href="{{ route('front.news.detail', $news->slug) }}">{{ $news->title }}</a></h4>
							<div class="blog-info">
								<a href="#"><i class="fa fa-user"></i>By: Admin</a>
								<a href="#"><i class="fa fa-clock-o"></i>{{ date('Y-m-d', strtotime($news->created_at)) }}</a>
							</div>
							<p>{{ strip_tags(Str::words($news->description, 18)) }}</p>
							<div class="button">
								<a href="{{ route('front.news.detail', $news->slug) }}" class="btn">Lanjut Baca<i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<!-- End Single Blog -->
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Blogs -->
@endsection