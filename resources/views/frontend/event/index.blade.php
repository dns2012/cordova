@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Acara - Sekolah Alam Cordova</title>
<meta name="title" content="Acara - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Acara - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Acara - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Acara - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Acara</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.event') }}">acara kami</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Events -->
<section class="events section">
    <div class="container">
        <div class="row">
            <div class="col-12 wow zoomIn">
                <div class="section-title">
                    <h2>Acara / Event <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($acara as $event)
            <div class="col-lg-4">
                <!-- Single Event -->
                <div class="single-event">
                    <div class="head overlay">
                        <img src="{{ $event->image }}" alt="#">
                        <a href="{{ $event->image }}" data-fancybox="photo" class="btn"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="event-content">
                        <div class="meta">
                            <span><i class="fa fa-calendar"></i>{{ date('d F Y', strtotime($event->date)) }}</span>
                            <span><i class="fa fa-clock-o"></i>{{ date('H.i A', strtotime($event->time)) }}</span>
                        </div>
                        <h4 style="height: 35px"><a href="{{ route('front.event.detail', $event->slug) }}">{{ $event->title }}</a></h4>
                        <p>{{ strip_tags(Str::words($event->description, 10)) }}</p>
                        <div class="button">
                            <a href="{{ route('front.event.detail', $event->slug) }}" class="btn">Ikuti Acara</a>
                        </div>
                    </div>
                </div>
                <!--/ End Single Event -->
            </div>
            @endforeach
        </div>
        <div class="row block-nav justify-content-center mt-5">
            <div class="col-lg-12 col-7 block-nav__box">
                {{ $acara->links() }}
            </div>
        </div>
    </div>
</section>
<!--/ End Events -->
@endsection