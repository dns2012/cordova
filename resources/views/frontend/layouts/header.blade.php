@section('header')
<!-- Header -->
<header class="header">
    <!-- Header Inner -->
    <div class="header-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-12">
                    <div class="logo">
                        <a href="{{ route('front.home') }}"><img src="{{ asset('learnedu') }}/images/logo-cordova.png" alt="#"></a>
                    </div>
                    <div class="mobile-menu"></div>
                </div>
                <div class="col-lg-9 col-md-9 col-12">
                    <!-- Header Widget -->
                    <div class="header-widget">
                        <div class="single-widget">
                            <i class="fa fa-phone"></i>
                            <h4>Telepon<span>(0335) 4494501</span></h4>
                        </div>
                        <div class="single-widget">
                            <i class="fa fa-envelope-o"></i>
                            <h4>Email<a href="mailto:mailus@mail.com"><span>support@salamcordova.sch.id</span></a></h4>
                        </div>
                        <div class="single-widget">
                            <i class="fa fa-map-marker"></i>
                            <h4>Lokasi<span>Jl. Sunan Giri RT.02 RW.05 Mantong Sumber Taman </span></h4>
                        </div>
                    </div>
                    <!--/ End Header Widget -->
                </div>
            </div>
        </div>
    </div>
    <!--/ End Header Inner -->
    <!-- Header Menu -->
    <div class="header-menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-default">
                        <div class="navbar-collapse">
                            <!-- Main Menu -->
                            <ul id="nav" class="nav menu navbar-nav">
                                <li class="{{ strpos(Route::currentRouteName(), 'home') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.home') }}"><i class="fa fa-home"></i> Beranda</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'about') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.about') }}"><i class="fa fa-group"></i> Profil</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'program') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.program') }}"><i class="fa fa-book"></i> Program</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'event') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.event') }}"><i class="fa fa-calendar"></i> Acara</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'news') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.news') }}"><i class="fa fa-newspaper-o"></i> Berita</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'gallery') === false ? '' : 'active' }}">
                                    <a href="javascript:void(0)"><i class="fa fa-image"></i> Galeri</a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('front.gallery.stage', 'TK') }}">TK</a></li>
                                        <li><a href="{{ route('front.gallery.stage', 'SD') }}">SD</a></li>
                                        <li><a href="{{ route('front.gallery.stage', 'SMP') }}">SMP</a></li>
                                    </ul>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'contact') === false ? '' : 'active' }}">
                                    <a href="{{ route('front.contact') }}"><i class="fa fa-map-marker"></i> Kontak</a>
                                </li>
                                <li class="{{ strpos(Route::currentRouteName(), 'pdb') === false ? '' : 'active' }} d-block d-sm-block d-md-none d-lg-none d-xl-none">
                                    <a href="{{ route('front.pdb.step-1') }}"><i class="fa fa-file"></i> PPDB 2020</a>
                                </li>
                                <!-- <li><a href="#">Blogs<i class="fa fa-angle-down"></i></a>
											<ul class="dropdown">
												<li><a href="blogs.html">Blogs Grid Layout</a></li>
												<li><a href="blogs-left-sidebar.html">Blogs Left Sidebar</a></li>
												<li><a href="blogs-right-sidebar.html">Blogs Right Sidebar</a></li>
												<li><a href="blog-single-left-sidebar.html">Blogs Single - Left Sidebar</a></li>
												<li><a href="#">Blogs Single - Right Sidebar</a></li>
											</ul>
										</li> -->
                            </ul>
                            <!-- End Main Menu -->
                            <!-- button -->
                            <div class="button">
                                <a href="{{ route('front.pdb.step-1') }}" class="btn"><i class="fa fa-pencil"></i>PPDB 2020</a>
                            </div>
                            <!--/ End Button -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--/ End Header Menu -->
</header>
<!-- End Header -->
@endsection