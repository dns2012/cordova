@section('footer')
<!-- Footer -->
<footer class="footer overlay section wow fadeIn">
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <!-- About -->
                    <div class="single-widget about">
                        <div class="logo"><a href="#"><img src="{{ asset('learnedu') }}/images/logo-cordova.png" alt="#"></a></div>
                        <p>Sekolah Alam Cordova pada dasarnya adalah bentuk pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan juga sebagai objek pembelajaran. Dengan konsep pendidikan ini para siswa diharapkan bisa belajar dari alam lingkungan sekitar dan mengaitkan pada pelajaran.</p>
                        <ul class="list">
                            <li><i class="fa fa-phone"></i>Telepon: (0335) 4494501</li>
                            <li><i class="fa fa-envelope"></i>Email: <a href="support@salamcordova.sch.id">support@salamcordova.sch.id</a></li>
                            <li><i class="fa fa-map-o"></i>Alamat: Jl.Sunan Giri RT.02 RW.05 Mantong, Sumber Taman, Wonoasih , Kota Probolinggo</li>
                        </ul>
                    </div>
                    <!--/ End About -->
                </div>
                <div class="col-lg-2 col-md-6 col-12">
                    <!-- Useful Links -->
                    <div class="single-widget useful-links">
                        <h2>Tautan</h2>
                        <ul>
                            <li><a href="{{ route('front.home') }}"><i class="fa fa-angle-right"></i>Home</a></li>
                            <li><a href="{{ route('front.about') }}"><i class="fa fa-angle-right"></i>Tentang Kami</a></li>
                            <li><a href="{{ route('front.program') }}"><i class="fa fa-angle-right"></i>Program Unggulan</a></li>
                            <li><a href="{{ route('front.event') }}"><i class="fa fa-angle-right"></i>Acara</a></li>
                            <li><a href="{{ route('front.news') }}"><i class="fa fa-angle-right"></i>Berita</a></li>
                            <li><a href="{{ route('front.gallery.stage', 'TK') }}"><i class="fa fa-angle-right"></i>Galeri</a></li>
                            <li><a href="{{ route('front.contact') }}"><i class="fa fa-angle-right"></i>Kontak</a></li>
                            <li><a href="{{ route('front.pdb.step-1') }}"><i class="fa fa-angle-right"></i>PPDB 2020</a></li>
                        </ul>
                    </div>
                    <!--/ End Useful Links -->
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <!-- Latest News -->
                    <div class="single-widget latest-news">
                        <h2>Acara Terbaru</h2>
                        <div class="news-inner">
                            @foreach ($events as $event)
                            <div class="single-news" onclick="window.location='{{ route('front.event.detail', $event->slug) }}'" style="cursor: pointer">
                                <img src="{{ $event->image}}" alt="" style="object-fit: cover">
                                <h4><a href="{{ route('front.event.detail', $event->slug) }}">{{ $event->title }}</a></h4>
                                <p>{{ strip_tags(Str::words($event->description, 10)) }}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!--/ End Latest News -->
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <!-- Newsletter -->
                    <div class="single-widget newsletter">
                        <h2>Subscribe Berita / Acara</h2>
                        <div class="mail">
                            <p>Jangan lewatkan pengetahuan atau acara terbaru kami !</p>
                            <div class="form">
                                <input type="email" placeholder="Masukkan email">
                                <button class="button" type="submit"><i class="fa fa-envelope"></i></button>
                            </div>
                        </div>
                    </div>
                    <!--/ End Newsletter -->
                </div>
            </div>
        </div>
    </div>
    <!--/ End Footer Top -->
    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bottom-head">
                        <div class="row">
                            <div class="col-12">
                                <!-- Social -->
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                                <!-- End Social -->
                                <!-- Copyright -->
                                <div class="copyright">
                                    <p>© Copyright 2020 <a href="#">Sekolah Alam Cordova</a>. All Rights Reserved</p>
                                </div>
                                <!--/ End Copyright -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ End Footer Bottom -->
</footer>
<!--/ End Footer -->
@endsection