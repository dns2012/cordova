@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">PPDB</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('news.index') }}">PPDB</a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ $student->name }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td>{{ $student->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <td>{{ $student->gender }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenjang Pendaftaran</th>
                                        <td>@if(! empty($student->stage)){{ $student->stage }}@else-@endif</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Induk Siswa Nasional</th>
                                        <td>{{ $student->nisn }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Induk Kependudukan</th>
                                        <td>{{ $student->nik }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tempat dan Tanggal Lahir</th>
                                        <td>{{ $student->birth_place }}, {{ $student->birth_date }}-{{ $student->birth_month }}-{{ $student->birth_year }}</td>
                                    </tr>
                                    <tr>
                                        <th>Agama</th>
                                        <td>{{ $student->religion }}</td>
                                    </tr>
                                    <tr>
                                        <th>Berkebutuhan Khusus</th>
                                        <td>{{ $student->disability }}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Tempat Tinggal</th>
                                        <td>
                                            {{ $student->address }} <br>
                                            <strong>Dusun</strong> {{ $student->village}} | <strong>RT</strong> {{ $student->rt}} | <strong>RW</strong> {{ $student->rw}} <br>
                                            <strong>Kelurahan / Desa</strong> {{ $student->village_office }} | <strong>Kode Pos</strong> {{ $student->postal_code }} <br>
                                            <strong>Kecamatan</strong> {{ $student->sub_district }} <br>
                                            <strong>Kabupaten / Kota</strong> {{ $student->city }} <br>
                                            <strong>Provinsi</strong> {{ $student->province }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kendaraan Sekolah</th>
                                        <td>{{ $student->vehicle }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Tinggal</th>
                                        <td>{{ $student->live }}</td>
                                    </tr>
                                    <tr>
                                        <th>Telepon Rumah</th>
                                        <td>@if (! empty($student->house_phone)){{ $student->house_phone}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>No. HP</th>
                                        <td>{{ $student->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>@if (! empty($student->email)){{ $student->email}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Penerima KPS</th>
                                        <td>
                                            {{ $student->kps }} <br>
                                            <strong>No KPS</strong> @if (! empty($student->kps_number)){{ $student->kps_number}}@else - @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kewarganegaraan</th>
                                        <td>{{ $student->nationality }}</td>
                                    </tr>
                                    <tr>
                                        <th>Usulan Layak PP</th>
                                        <td>{{ $student->pp }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bank</th>
                                        <td>@if (! empty($student->bank)){{ $student->bank}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>No Rekening Bank</th>
                                        <td>@if (! empty($student->bank_account)){{ $student->bank_account}}@else - @endif</td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Nama Ayah</th>
                                        <td>{{ $student->father_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun Lahir</th>
                                        <td>{{ $student->father_birth_year }}</td>
                                    </tr>
                                    <tr>
                                        <th>Berkebutuhan Khusus</th>
                                        <td>{{ $student->father_disability }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pekerjaan</th>
                                        <td>{{ $student->father_job }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pendidikan</th>
                                        <td>{{ $student->father_education }}</td>
                                    </tr>
                                    <tr>
                                        <th>Penghasilan Bulanan</th>
                                        <td>{{ $student->father_salary }}</td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Nama Ibu</th>
                                        <td>{{ $student->mother_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun Lahir</th>
                                        <td>{{ $student->mother_birth_year }}</td>
                                    </tr>
                                    <tr>
                                        <th>Berkebutuhan Khusus</th>
                                        <td>{{ $student->mother_disability }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pekerjaan</th>
                                        <td>{{ $student->mother_job }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pendidikan</th>
                                        <td>{{ $student->mother_education }}</td>
                                    </tr>
                                    <tr>
                                        <th>Penghasilan Bulanan</th>
                                        <td>{{ $student->mother_salary }}</td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Nama Wali</th>
                                        <td>@if (! empty($student->guardian_name)){{ $student->guardian_name}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun Lahir</th>
                                        <td>@if (! empty($student->guardian_birth_year)){{ $student->guardian_birth_year}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Berkebutuhan Khusus</th>
                                        <td>@if (! empty($student->guardian_disability)){{ $student->guardian_disability}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Pekerjaan</th>
                                        <td>@if (! empty($student->guardian_job)){{ $student->guardian_job}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Pendidikan</th>
                                        <td>@if (! empty($student->guardian_education)){{ $student->guardian_education}}@else - @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Penghasilan Bulanan</th>
                                        <td>@if (! empty($student->guardian_salary)){{ $student->guardian_salary}}@else - @endif</td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Tinggi Badan</th>
                                        <td>{{ $student->height }} cm</td>
                                    </tr>
                                    <tr>
                                        <th>Berat Badan</th>
                                        <td>{{ $student->weight }} cm</td>
                                    </tr>
                                    <tr>
                                        <th>Jarak Tempat Tinggal ke Sekolah</th>
                                        <td>{{ $student->distance }} km</td>
                                    </tr>
                                    <tr>
                                        <th>Waktu Tempuh</th>
                                        <td>{{ $student->distance_time }} menit</td>
                                    </tr>
                                    <tr>
                                        <th>Jumlah Saudara Kandung</th>
                                        <td>{{ $student->brother }}</td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Catatan Prestasi</th>
                                        <td>
                                            @if (! empty($achievements))
                                            <ol>
                                                @foreach ($student->achievements as $achievement)
                                                <li class="mb-2">
                                                    <strong>Jenis Prestasi</strong> : @if (! empty($achievement->kind)){{ $achievement->kind}}@else - @endif <br>
                                                    <strong>Tingkat</strong> : @if (! empty($achievement->level)){{ $achievement->level}}@else - @endif <br>
                                                    <strong>Nama Prestasi</strong> : @if (! empty($achievement->name)){{ $achievement->name}}@else - @endif <br>
                                                    <strong>Tahun</strong> : @if (! empty($achievement->year)){{ $achievement->year}}@else - @endif <br>
                                                    <strong>Penyelenggara</strong> : @if (! empty($achievement->organizer)){{ $achievement->organizer}}@else - @endif <br>
                                                </li>
                                                @endforeach
                                            </ol>
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="pdb-separator-tr">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Beasiswa</th>
                                        <td>
                                            @if (! empty($scholarships))
                                            <ol>
                                                @foreach ($student->scholarships as $scholarship)
                                                <li class="mb-2">
                                                    <strong>Jenis</strong> : @if (! empty($scholarship->kind)){{ $scholarship->kind}}@else - @endif <br>
                                                    <strong>Penyelenggara / Sumber</strong> : @if (! empty($scholarship->organizer)){{ $scholarship->organizer}}@else - @endif <br>
                                                    <strong>Tahun Mulai</strong> : @if (! empty($scholarship->year_start)){{ $scholarship->year_start}}@else - @endif <br>
                                                    <strong>Tahun Selesai</strong> : @if (! empty($scholarship->year_finish)){{ $scholarship->year_finish}}@else - @endif <br>
                                                </li>
                                                @endforeach
                                            </ol>
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                </table>

                                <button class="btn btn-primary" onclick="window.open('{{ route('pdb.print', $model->id) }}', '_blank')">CETAK PDF</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sticky no-padding" title="Add New" onclick="window.location='{{ route('news.create') }}'">
        <i class="ft-plus la-3x"></i>
    </button>
</div>
@endsection