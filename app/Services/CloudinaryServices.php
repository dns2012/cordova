<?php 

namespace App\Services;

use Cloudinary\Uploader as CloudinaryUpload;

class CloudinaryService
{

    public static function upload($image)
    {
        return CloudinaryUpload::upload($image, [
            'folder' => 'cordova'
        ])['secure_url'];
    }

    public static function remove($image)
    {
        $currentImage = explode('/', $image);
        $currentImageFileName = explode('.', end($currentImage));
        $publicId = 'cordova/' .  $currentImageFileName[0];
        return CloudinaryUpload::destroy($publicId);
    }
}