@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>{{ $event->title }}</title>
<meta name="title" content="{{ $event->title }}">
<meta name="description" content="{{ Str::words($event->description, 80) }}">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="{{ $event->title }}">
<meta itemprop="description" content="{{ Str::words($event->description, 80) }}">
<meta itemprop="image" content="{{ $event->image }}">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{ $event->title }}">
<meta property="og:description" content="{{ Str::words($event->description, 80) }}">
<meta property="og:image" content="{{ $event->image }}">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{ $event->title }}">
<meta name="twitter:description" content="{{ Str::words($event->description, 80) }}">
<meta name="twitter:image" content="{{ $event->image }}">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Acara</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="{{ route('front.event') }}">acara kami<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.event.detail', $event->slug) }}">{{ $event->title }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses section-bg section">
    <div class="container">
        <div class="row">
            <div class="col-12 wow zoomIn">
                <div class="section-title">
                    <h2><span>{{ $event->title }}</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-lg-10">
                <div style="width: auto: height: auto" class="mb-4">
                    <img src="{{ $event->image }}" alt="{{ $event->slug }}" style="width: 100%; height: 100%; object-fit: cover">
                </div>

                <div class="meta mb-3" style="font-weight: bold">
                    <span><i class="fa fa-calendar"></i> {{ date('d F Y', strtotime($event->date)) }}</span>
                    <span><i class="fa fa-clock-o"></i> {{ date('H.i A', strtotime($event->time)) }}</span>
                </div>

                {!! $event->description !!}
            </div>
        </div>
    </div>
</section>
<!--/ End Courses -->

@endsection