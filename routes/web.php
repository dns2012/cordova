<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/dashboard', 'Admin\DashboardController@index')
    ->name('dashboard')
    ->middleware(['auth']);

Route::resource('admin', 'Admin\AdminController')
    ->middleware('auth');

Route::resource('banner', 'Admin\BannerController')
    ->middleware('auth');

Route::resource('about', 'Admin\AboutController')
    ->middleware('auth')
    ->only(['edit', 'update']);

Route::resource('program', 'Admin\ProgramController')
    ->middleware('auth');

Route::resource('teacher', 'Admin\TeacherController')
    ->middleware('auth');

Route::resource('event', 'Admin\EventController')
    ->middleware('auth');

Route::resource('news', 'Admin\NewsController')
    ->middleware('auth');

Route::resource('gallery', 'Admin\GalleryController')
    ->middleware('auth');

Route::get('gallery-stage/{stage}', 'Admin\GalleryController@indexStage')
    ->name('gallery.stage')
    ->middleware('auth');

Route::prefix('pdb')
    ->name('pdb.')
    ->middleware(['auth'])
    ->group(function () {

        Route::get('manage/list/{status}/{stage}', 'Admin\RegisterController@index')
            ->name('index');

        Route::get('manage/detail/{id}', 'Admin\RegisterController@detail')
            ->name('detail');

        Route::get('manage/print/{id}', 'Admin\RegisterController@print')
            ->name('print');

        Route::put('manage/process/{id}', 'Admin\RegisterController@process')
            ->name('process');
    });




Route::get('/', 'Frontend\HomeController@index')
    ->name('front.home');

Route::get('profil-kami', 'Frontend\AboutController@index')
    ->name('front.about');

Route::get('program-unggulan', 'Frontend\ProgramController@index')
    ->name('front.program');

Route::get('program-unggulan/{slug}', 'Frontend\ProgramController@detail')
    ->name('front.program.detail');

Route::get('acara', 'Frontend\EventController@index')
    ->name('front.event');

Route::get('acara/{slug}', 'Frontend\EventController@detail')
    ->name('front.event.detail');

Route::get('berita', 'Frontend\NewsController@index')
    ->name('front.news');

Route::get('berita/{slug}', 'Frontend\NewsController@detail')
    ->name('front.news.detail');

Route::get('galeri', 'Frontend\GalleryController@index')
    ->name('front.gallery');

Route::get('galeri/{stage}', 'Frontend\GalleryController@indexStage')
    ->name('front.gallery.stage');

Route::get('kontak', 'Frontend\ContactController@index')
    ->name('front.contact');

Route::prefix('pdb')
    ->name('front.pdb.')
    ->group(function () {

        Route::get('step-1', 'Frontend\RegisterController@stepOne')
            ->name('step-1');

        Route::get('step-2', 'Frontend\RegisterController@stepTwo')
            ->name('step-2');

        Route::get('step-3', 'Frontend\RegisterController@stepThree')
            ->name('step-3');

        Route::get('step-4', 'Frontend\RegisterController@stepFour')
            ->name('step-4');

        Route::get('step-5', 'Frontend\RegisterController@stepFive')
            ->name('step-5');

        Route::get('step-6', 'Frontend\RegisterController@stepSix')
            ->name('step-6');

        Route::get('complete', 'Frontend\RegisterController@complete')
            ->name('complete');

        Route::post('store/{step}', 'Frontend\RegisterController@store')
            ->name('store');
    });
