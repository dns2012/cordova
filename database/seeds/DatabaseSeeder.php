<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(ProgramSeeder::class);
        $this->call(TeacherSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(GallerySeeder::class);
        $this->call(PdbSeeder::class);
    }
}
