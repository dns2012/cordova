<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Event;
use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        return view('frontend.gallery.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'galleries' => Gallery::orderBy('id', 'DESC')->paginate(9)
        ]);
    }

    public function indexStage($stage)
    {
        if (! in_array($stage, ['TK', 'SD', 'SMP'])) {
            abort(404);
        }

        return view('frontend.gallery.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'galleries' => Gallery::where('stage', $stage)->orderBy('id', 'DESC')->paginate(9),
            'stage' => $stage
        ]);
    }
}
