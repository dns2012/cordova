<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	@yield('meta')
	
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{ asset('learnedu') }}/images/icon.png">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/bootstrap.min.css?v=2">
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/font-awesome.min.css?v=2">
	<!-- Fancy Box CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/jquery.fancybox.min.css?v=2">
	<!-- Owl Carousel CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/owl.carousel.min.css?v=2">
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/owl.theme.default.min.css?v=2">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/animate.min.css?v=2">
	<!-- Slick Nav CSS -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/slicknav.min.css?v=2">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/magnific-popup.css?v=2">
	<!-- Stepper -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/bs-stepper.min.css?v=2">

	<!-- Cordova Stylesheet -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/normalize.css?v=2">
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/style.css?v=3">
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/responsive.css?v=2">

	<!-- Cordova Color -->
	<link rel="stylesheet" href="{{ asset('learnedu') }}/css/color1.css?v=2">
</head>

<body>

	@yield('header')

	@yield('content')

	@yield('footer')

	<!-- Jquery JS-->
	<script src="{{ asset('learnedu') }}/js/jquery.min.js"></script>
	<script src="{{ asset('learnedu') }}/js/jquery-migrate.min.js"></script>
	<!-- Popper JS-->
	<script src="{{ asset('learnedu') }}/js/popper.min.js"></script>
	<!-- Bootstrap JS-->
	<script src="{{ asset('learnedu') }}/js/bootstrap.min.js"></script>
	<!-- Colors JS-->
	<script src="{{ asset('learnedu') }}/js/colors.js"></script>
	<!-- Jquery Steller JS -->
	<script src="{{ asset('learnedu') }}/js/jquery.stellar.min.js"></script>
	<!-- Particle JS -->
	<script src="{{ asset('learnedu') }}/js/particles.min.js"></script>
	<!-- Fancy Box JS-->
	<script src="{{ asset('learnedu') }}/js/facnybox.min.js"></script>
	<!-- Magnific Popup JS-->
	<script src="{{ asset('learnedu') }}/js/jquery.magnific-popup.min.js"></script>
	<!-- Masonry JS-->
	<script src="{{ asset('learnedu') }}/js/masonry.pkgd.min.js"></script>
	<!-- Circle Progress JS -->
	<script src="{{ asset('learnedu') }}/js/circle-progress.min.js"></script>
	<!-- Owl Carousel JS-->
	<script src="{{ asset('learnedu') }}/js/owl.carousel.min.js"></script>
	<!-- Waypoints JS-->
	<script src="{{ asset('learnedu') }}/js/waypoints.min.js"></script>
	<!-- Slick Nav JS-->
	<script src="{{ asset('learnedu') }}/js/slicknav.min.js"></script>
	<!-- Counter Up JS -->
	<script src="{{ asset('learnedu') }}/js/jquery.counterup.min.js"></script>
	<!-- Easing JS-->
	<script src="{{ asset('learnedu') }}/js/easing.min.js"></script>
	<!-- Wow Min JS-->
	<script src="{{ asset('learnedu') }}/js/wow.min.js"></script>
	<!-- Scroll Up JS-->
	<script src="{{ asset('learnedu') }}/js/jquery.scrollUp.min.js"></script>
	<!-- Stepper JS -->
	<script src="{{ asset('learnedu') }}/js/bs-stepper.min.js"></script>
	<!-- Main JS-->
	<script src="{{ asset('learnedu') }}/js/main.js"></script>
</body>

</html>

@yield('script')