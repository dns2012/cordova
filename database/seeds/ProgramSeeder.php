<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            [
                'title' => 'Tahsin & Tahfidz Quran',
                'description' => 'Aktifitas harian siswa sehingga diharapkan seluruh siswa mampu membaca al-quran sesuai dengan levelnya.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/program-1.jpg',
                'slug' => 'tahsin-tahfidz-quran'
            ],
            [
                'title' => 'Science Project',
                'description' => 'Project karya yang dirancang, dibuat dan dipresentasikan oleh siswa hingga dipasarkan kepada konsumen.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/program-2.jpg',
                'slug' => 'science-project'
            ],
            [
                'title' => 'Ekstrakurikuler Berkualitas',
                'description' => 'Kegiatan tambahan untuk mengasah potensi bakat agar memiliki softskill dan prestasi.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/program-3.jpg',
                'slug' => 'ekstrakurikuler-berkualitas'
            ],
        ]);
    }
}
