<?php

namespace App\Http\Controllers\Admin;

use App\About;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAbout;
use App\Services\ImgurService;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.about.edit', [
            'about' => About::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAbout $request, $id)
    {
        $request->validated();

        $about = About::findOrFail($id);
        $about->prologue = $request->input('prologue');
        $about->vision = $request->input('vision');
        $about->mission = $request->input('mission');
        $about->point = json_encode([
            'excellent_program' => $request->input('excellent_program'),
            'active_student' => $request->input('active_student'),
            'professional_teacher' => $request->input('professional_teacher'),
            'award_received' => $request->input('award_received')
        ]);

        if($about->save()) {
            return redirect()->route('about.edit', 1);
        }
    }
}
