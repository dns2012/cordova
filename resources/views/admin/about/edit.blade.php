@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Tentang Kami</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('about.edit', 1) }}">Tentang Kami</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Edit
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-body">
                                <form method="POST" action="{{ route('about.update', $about->id) }}" enctype="multipart/form-data" class="forms-sample">
                                    @csrf
                                    @method ('PUT')
                                    <div class="form-group">
                                        <label>Prologue / History / Introduction</label>
                                        <textarea name="prologue" class="form-control @error('prologue') is-invalid @enderror" placeholder="Prologue" required>
                                        {{ $about->prologue }}
                                        </textarea>
                                        @error('prologue')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Vision</label>
                                        <textarea name="vision" class="form-control @error('vision') is-invalid @enderror" placeholder="Vision" required>
                                        {{ $about->vision }}
                                        </textarea>
                                        @error('vision')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Mission</label>
                                        <textarea name="mission" class="form-control @error('mission') is-invalid @enderror" placeholder="Mission" required>
                                        {{ $about->mission }}
                                        </textarea>
                                        @error('mission')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="row">
                                        @php $point = json_decode($about->point) @endphp
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Excellent Program</label>
                                                <input type="number" name="excellent_program" class="form-control @error('excellent_program') is-invalid @enderror" value="{{ $point->excellent_program }}">
                                                @error('excellent_program')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Active Student</label>
                                                <input type="number" name="active_student" class="form-control @error('active_student') is-invalid @enderror" value="{{ $point->active_student }}">
                                                @error('active_student')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Professional Teacher</label>
                                                <input type="number" name="professional_teacher" class="form-control @error('professional_teacher') is-invalid @enderror" value="{{ $point->professional_teacher }}">
                                                @error('professional_teacher')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Award Received</label>
                                                <input type="number" name="award_received" class="form-control @error('professional_teacher') is-invalid @enderror" value="{{ $point->award_received }}">
                                                @error('award_received')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-rounded mr-2">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection