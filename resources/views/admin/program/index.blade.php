@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Program</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('program.index') }}">Program</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Index
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height">
                @foreach ($programs as $program)
                <div class="col-lg-3 col-md-3">
                    <div class="card">
                        <img class="block-program__card-image" src="{{ $program->image }}" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title block-program__card-body-title">{{ $program->title }}</h4>
                            <h6 class="card-subtitle text-muted block-program__card-body-subtitle">{{ strip_tags(Str::words($program->description, 15)) }}</h6>
                        </div>
                        <div class="card-footer block-program__card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                            <div class="block-program__card-footer-grid">
                                <div class="block-program__card-footer-grid-item">
                                    <button class="btn btn-info full-width" title="Edit" onclick="window.location='{{ route('program.edit', $program->id) }}'">
                                        <i class="ft-edit"></i> Edit
                                    </button>
                                </div>
                                <div class="block-program__card-footer-grid-item">
                                    <form action="{{ route('program.destroy', $program->id) }}" method="POST" onsubmit="return confirm('Are you sure?')">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger full-width" title="Delete">
                                            <i class="ft-trash"></i> Delete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row block-nav justify-content-center">
                <div class="col-lg-12 col-7 block-nav__box">
                   {{ $programs->links() }}
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sticky no-padding" title="Add New" onclick="window.location='{{ route('program.create') }}'">
        <i class="ft-plus la-3x"></i>
    </button>
</div>
@endsection