<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            [
                'title' => 'Aku Ingin Menjadi Fuqaha, Bukan "hanya" Penghafal Qur`an',
                'description' => 'Kalau sekedar hafal, kaset murattal pun bisa untuk belajar. Kalau sekedar pengetahuan, di Google pun sangat banyak bahan yang dapat kita baca, lengkap dengan segala kesalahan dan kerusakannya.
    
                Tetapi sungguh, ada amanah berat yang harus dipikul oleh para penghafal Al-Qur"an. Ada tugas besar yang menanti para pelajar agama ini. Bagi mereka, ada akhlak yang harus tumbuh tertanam kuat dalam diri. Ada adab yang harus ditegakkan. Pada masing-masing mereka, ada ruh yang harus diserap dari setiap ayat dan ilmu.
                
                Maka jika lembaga yang mengajarkan menghafal Al-Qur"an sibuk mengejar cepatnya menghafal, sementara adab kurang diperhatikan, sungguh aku sangat khawatir terhadap mereka. Itu sebabnya, memasukkan anak ke pesantren tahfidz–apalagi sekedar sekolah yang menambahkan kegiatan menghafal–harus lebih cermat lagi. Ini merupakan perkara yang sangat besar. Maka jangan asal dapat menghafal dengan cepat.
                
                Terngiang-ngiang nasehat seorang kyai di Pamekasan, Madura, tentang mengapa beliau menghentikan program tahfidz di pesantren yang dipimpinnya. Bukan tak ada lagi yang diizinkan menghafal Al-Qur"an. Bukan. Tetapi hanya apabila tampak adab dan komitmennya kepada Al-Qur"an, barulah dibimbing menghafal. Itu pun bukan atas dasar permintaan santri, melainkan lebih kepada kepatutan dari segi adab dan komitmen.
                
                "Cinta dunia dan Al-Qur"an tidak dapat bersatu dalam hati manusia," kata beliau, "Jika dunia dalam hatinya, Al-Qur"an hanya menjadi alatnya." | Tetapi jika adabnya kepada Al-Qur"an tertanam kuat, komitmennya sangat besar, Al-Qur"an akan menjadi pegangan yang memandu hidupnya. Bukan sebaliknya.
                
                Khusus mengenai menghafal Al-Qur"an, penting diingat bahwa ada peringatan mengenai orang yang hanya menghafal saja. Di antara tanda-tanda zaman fitnah ialah, semakin banyaknya penghafal Al-Qur"an, tetapi semakin sedikit fuqaha-nya. Siapa fuqaha? Orang yang memiliki pemahaman sangat mendalam tentang agama. Nah, di antara tanda zaman fitnah, penghafal Al-Qur"an menjamur, fuqaha semakin sedikit.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/news-1.jpg',
                'pageview' => 0,
                'slug' => 'aku-ingin-menjadi-fuqaha-bukan-hanya-penghafal-quran'
            ],
            [
                'title' => 'JANGAN TERBURU-BURU MENDIDIK ANAK',
                'description' => 'Anies ke Guru PAUD: Jangan Paksa Anak Bisa Baca Tulis Sejak Dini

                Gubernur DKI Jakarta Anies Baswedan meresmikan PAUD Setya Bakti, Pondok Kopi, Jakarta Timur. Foto: Paulina Herasmaranindar/kumparan
                
                Gubernur DKI Jakarta, Anies Baswedan, meresmikan Pendidikan Anak Usia Dini (PAUD) Setya Bakti, yang merupakan bagian dari program bedah seribu PAUD yang diinisiasi Bunda Pintar Indonesia (BPI). Dalam sambutannya, Anies menitipkan pesan agar setiap guru PAUD dapat menghadirkan suasana menyenangkan bagi anak-anak.
                
                "Saya juga titip pada ibu-ibu di sini di PAUD Setya Bakti, pastikan anak-anak belajar dengan senang dan bahagia. Tidak perlu diajari baca tulis," kata Anies di Pondok Kopi, Jakarta Timur, Sabtu (28/9).
                
                Selain itu, Anies juga berharap agar guru tak memaksakan anak untuk belajar baca dan tulis di lingkup PAUD. Ia ingin agar guru tak mengikuti keinginan orang tua yang menginginkan anaknya dapat menulis dan membaca sejak dini.
                
                "Jangan kalau ada orang tua memaksa. Paling sulit guru PAUD menghadapi keinginan orang tua agar anaknya bisa baca tulis," tutur dia.
                
                Mantan Mendikbud itu menceritakan pengalamannya yang baru belajar membaca dan menulis saat duduk di bangku sekolah dasar (SD). Untuk itu, ia meminta setiap orang tua tak perlu merasa khawatir jika anaknya belum bisa baca dan tulis saat PAUD.
                
                "Saya cerita pengalaman saya. Saya ini masuk sekolah 3 tahun. Masuk SD 7 tahun. Jadi PAUD dan TK saya 4 tahun. Belajar baca tulis itu baru SD. Saya ingat persis belajar baca tulis ya baru saat itu kelas 1 (SD). Jadi selama 4 tahun ya bermain saja," ungkapnya.
                
                Gubernur DKI Jakarta Anies Baswedan meresmikan PAUD Setya Bakti, Pondok Kopi, Jakarta Timur. Foto: Paulina Herasmaranindar/kumparan
                
                "Saya terlambat, tapi alhamdulillah sekarang bisa baca tulis, jangan khawatir. Enggak jelek-jelek amat kalau nulis. Jadi kalau ada yang memaksa baca tulis, ini bisa jadi contoh. Saya 4 tahun ikut PAUD, tidak bisa baca tulis, gedenya saya tidak ketinggalan. Orang tua jangan khawatir. Guru-guru harus jelaskan, justru salah kalau anak ini dipaksa baca tulis dari awal," imbuh dia.
                
                Dalam kesempatan yang sama, Pemimpin BPI yang juga anggota DPRD DKI Fraksi PAN, Zita Anjani, menuturkan ibu-ibu penggerak PAUD juga memberikan gelar kepada Anies dianggap sebagai ayah PAUD.
                
                "Selamat bertugas juga untuk Ayah PAUD kita, Bapak Gubernur Anies. Kalau penggerak PAUD kan Ibu-Ibu mayoritas. Nah, kalau yang bapak-bapak kita panggil Ayah PAUD, yaitu seseorang yang komit untuk ikut bekerja untuk pendidikan usia dini," kata dia.
                
                Zita berharap nantinya legislatif dan eksekutif dapat berkolaborasi mewujudkan regulasi yang dapat mempercepat perkembangan pendidikan usia dini. Ia ingin penyelenggaraan PAUD mendapatkan payung hukum yang jelas.
                
                "Paling tidak, kita mulai berjuang di DKI Jakarta dulu. Jika kita berhasil, Indonesia pasti akan terinspirasi. Kalau kita gagal, Indonesia bisa jadi pesimis. Saya ingin Perda atau Payung Hukum mengenai PAUD ini dapat jadi akselarasi pekerjaan kita," pungkasnya.',
                'image' => 'https://salamcordova.sch.id/learnedu/images/news-3.jpg',
                'pageview' => 0,
                'slug' => 'jangan-terburu-buru-mendidik-anak'
            ],
        ]);
    }
}
