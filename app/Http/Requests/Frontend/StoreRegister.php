<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->has('name')) {
            $rules = [
                'name' => ['required', 'string', 'max:255'],
                'gender' => ['required', 'string', 'max:255'],
                'stage' => ['required', 'string', 'max:255'],
                'nisn' => ['required', 'numeric'],
                'nik' => ['required', 'numeric'],
                'birth_place' => ['required', 'string', 'max:255'],
                'birth_date' => ['required', 'numeric', 'max:31'],
                'birth_month' => ['required', 'numeric', 'max:12'],
                'birth_year' => ['required', 'numeric'],
                'religion' => ['required', 'string', 'max:255'],
                'disability' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'village' => ['nullable', 'string', 'max:255'],
                'rt' => ['required', 'numeric'],
                'rw' => ['required', 'numeric'],
                'village_office' => ['nullable', 'string', 'max:255'],
                'postal_code' => ['required', 'numeric'],
                'sub_district' => ['required', 'string', 'max:255'],
                'city' => ['required', 'string', 'max:255'],
                'province' => ['required', 'string', 'max:255'],
                'vehicle' => ['required', 'string', 'max:255'],
                'live' => ['required', 'string', 'max:255'],
                'house_phone' => ['nullable', 'numeric'],
                'phone' => ['required', 'numeric'],
                'email' => ['nullable', 'email:rfc'],
                'kps' => ['required', 'string', 'max:255'],
                'kps_number' => ['nullable', 'numeric'],
                'nationality' => ['required', 'string', 'max:255'],
                'pp' => ['required', 'string', 'max:255'],
                'bank' => ['nullable', 'string', 'max:255'],
                'bank_account' => ['nullable', 'numeric']
            ];
        } elseif ($request->has('father_name')) {
            $rules = [
                'father_name' => ['required', 'string', 'max:255'],
                'father_birth_year' => ['required', 'numeric'],
                'father_disability' => ['required', 'string', 'max:255'],
                'father_job' => ['required', 'string', 'max:255'],
                'father_education' => ['required', 'string', 'max:255'],
                'father_salary' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_birth_year' => ['required', 'numeric'],
                'mother_disability' => ['required', 'string', 'max:255'],
                'mother_job' => ['required', 'string', 'max:255'],
                'mother_education' => ['required', 'string', 'max:255'],
                'mother_salary' => ['required', 'string', 'max:255'],
                'guardian_name' => ['nullable', 'string', 'max:255'],
                'guardian_birth_year' => ['nullable', 'numeric'],
                'guardian_disability' => ['nullable', 'string', 'max:255'],
                'guardian_job' => ['nullable', 'string', 'max:255'],
                'guardian_education' => ['nullable', 'string', 'max:255'],
                'guardian_salary' => ['nullable', 'string', 'max:255'],
            ];
        } elseif ($request->has('height')) {
            $rules = [
                'height' => ['required', 'numeric'],
                'weight' => ['required', 'numeric'],
                'distance' => ['required', 'numeric'],
                'distance_time' => ['required', 'numeric'],
                'brother' => ['required', 'numeric']
            ];
        } elseif ($request->has('achievements')) {
            $rules = [
                'achievements.*.kind' => ['nullable', 'string', 'max:255'],
                'achievements.*.level' => ['nullable', 'string', 'max:255'],
                'achievements.*.name' => ['nullable', 'string', 'max:255'],
                'achievements.*.year' => ['nullable', 'numeric'],
                'achievements.*.organizer' => ['nullable', 'string', 'max:255']
            ];
        } elseif ($request->has('scholarships')) {
            $rules = [
                'scholarships.*.kind' => ['nullable', 'string', 'max:255'],
                'scholarships.*.organizer' => ['nullable', 'string', 'max:255'],
                'scholarships.*.year_start' => ['nullable', 'numeric'],
                'scholarships.*.year_finish' => ['nullable', 'numeric']
            ];
        } else {
            $rules = [
                'confirm' => ['required', 'numeric', 'max:1']
            ];
        }

        return $rules;
    }
}
