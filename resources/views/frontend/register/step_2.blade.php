<div class="form-group">
    <div class="row">
        <div class="col-lg-9 mb-2">
            <label>Nama Ayah</label>
            <input type="text" name="father_name" value="@if(! empty($student->father_name)){{ $student->father_name }}@endif" required>
            @error('father_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-3">
            <label>Tahun Lahir</label>
            <select name="father_birth_year" required>
                <option value="">--</option>
                @for ($i=1970;$i<=2020;$i++) <option value="{{ $i }}" @if (! empty($student->father_birth_year) && $student->father_birth_year==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('father_birth_year')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-12 mb-2">
            <label>Berkebutuhan Khusus</label>
            <input type="text" name="father_disability" value="@if(! empty($student->father_disability)){{ $student->father_disability }}@endif">
            @error('father_disability')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pekerjaan</label>
            <select name="father_job" required>
                <option value="">--</option>
                @foreach ($jobs as $father_job)
                <option value="{{ $father_job }}" @if (! empty($student->father_job) && $student->father_job==$father_job) selected @endif>{{ $father_job }}</option>
                @endforeach
            </select>
            @error('father_job')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pendidikan</label>
            <select name="father_education" required>
                <option value="">--</option>
                @foreach ($educations as $father_education)
                <option value="{{ $father_education }}" @if (! empty($student->father_education) && $student->father_education==$father_education) selected @endif>{{ $father_education }}</option>
                @endforeach
            </select>
            @error('father_education')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Penghasilan Bulanan</label>
            <select name="father_salary" required>
                <option value="">--</option>
                @foreach ($salaries as $father_salary)
                <option value="{{ $father_salary }}" @if (! empty($student->father_salary) && $student->father_salary==$father_salary) selected @endif>{{ $father_salary }}</option>
                @endforeach
            </select>
            @error('father_salary')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-lg-9 mb-2">
            <label>Nama Ibu</label>
            <input type="text" name="mother_name" value="@if(! empty($student->mother_name)){{ $student->mother_name }}@endif" required>
            @error('mother_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-3">
            <label>Tahun Lahir</label>
            <select name="mother_birth_year" required>
                <option value="">--</option>
                @for ($i=1970;$i<=2020;$i++) <option value="{{ $i }}" @if (! empty($student->mother_birth_year) && $student->mother_birth_year==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('mother_birth_year')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-12 mb-2">
            <label>Berkebutuhan Khusus</label>
            <input type="text" name="mother_disability" value="@if(! empty($student->mother_disability)){{ $student->mother_disability }}@endif">
            @error('mother_disability')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pekerjaan</label>
            <select name="mother_job" required>
                <option value="">--</option>
                @foreach ($jobs as $mother_job)
                <option value="{{ $mother_job }}" @if (! empty($student->mother_job) && $student->mother_job==$mother_job) selected @endif>{{ $mother_job }}</option>
                @endforeach
            </select>
            @error('mother_job')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pendidikan</label>
            <select name="mother_education" required>
                <option value="">--</option>
                @foreach ($educations as $mother_education)
                <option value="{{ $mother_education }}" @if (! empty($student->mother_education) && $student->mother_education==$mother_education) selected @endif>{{ $mother_education }}</option>
                @endforeach
            </select>
            @error('mother_education')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Penghasilan Bulanan</label>
            <select name="mother_salary" required>
                <option value="">--</option>
                @foreach ($salaries as $mother_salary)
                <option value="{{ $mother_salary }}" @if (! empty($student->mother_salary) && $student->mother_salary==$mother_salary) selected @endif>{{ $mother_salary }}</option>
                @endforeach
            </select>
            @error('mother_salary')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-lg-9 mb-2">
            <label>Nama Wali</label>
            <input type="text" name="guardian_name" value="@if(! empty($student->guardian_name)){{ $student->guardian_name }}@endif">
            @error('guardian_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-3">
            <label>Tahun Lahir</label>
            <select name="guardian_birth_year">
                <option value="">--</option>
                @for ($i=1970;$i<=2020;$i++) <option value="{{ $i }}" @if (! empty($student->guardian_birth_year) && $student->guardian_birth_year==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('guardian_birth_year')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-12 mb-2">
            <label>Berkebutuhan Khusus</label>
            <input type="text" name="guardian_disability" value="@if(! empty($student->guardian_disability)){{ $student->guardian_disability }}@endif">
            @error('guardian_disability')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pekerjaan</label>
            <select name="guardian_job">
                <option value="">--</option>
                @foreach ($jobs as $guardian_job)
                <option value="{{ $guardian_job }}" @if (! empty($student->guardian_job) && $student->guardian_job==$guardian_job) selected @endif>{{ $guardian_job }}</option>
                @endforeach
            </select>
            @error('guardian_job')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Pendidikan</label>
            <select name="guardian_education">
                <option value="">--</option>
                @foreach ($educations as $guardian_education)
                <option value="{{ $guardian_education }}" @if (! empty($student->guardian_education) && $student->guardian_education==$guardian_education) selected @endif>{{ $guardian_education }}</option>
                @endforeach
            </select>
            @error('guardian_education')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Penghasilan Bulanan</label>
            <select name="guardian_salary">
                <option value="">--</option>
                @foreach ($salaries as $guardian_salary)
                <option value="{{ $guardian_salary }}" @if (! empty($student->guardian_salary) && $student->guardian_salary==$guardian_salary) selected @endif>{{ $guardian_salary }}</option>
                @endforeach
            </select>
            @error('guardian_salary')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>