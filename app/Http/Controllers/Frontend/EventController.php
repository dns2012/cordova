<?php

namespace App\Http\Controllers\Frontend;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        return view('frontend.event.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'acara' => Event::orderBy('id', 'DESC')->paginate(9),
        ]);
    }

    public function detail($slug = null)
    {
        return view('frontend.event.detail', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'event' => Event::where('slug', $slug)->firstOrFail() 
        ]);
    }
}
