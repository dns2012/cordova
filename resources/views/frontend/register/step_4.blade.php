@for ($u=0;$u<=3;$u++) <div class="form-group">
    <div class="row">
        <div class="col-lg-12 mb-2">
            <label>Jenis Prestasi</label>
            <input type="text" name="achievements[{{ $u }}][kind]" value="@if(! empty($student->achievements[$u]->kind)){{ $student->achievements[$u]->kind }}@endif">
            @error('achievements.'.$u.'.kind')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="row mt-3 ml-1 mr-1">
        <div class="col-lg-12 mb-2">
            <label>Tingkat</label>
            <input type="text" name="achievements[{{ $u }}][level]" value="@if(! empty($student->achievements[$u]->level)){{ $student->achievements[$u]->level }}@endif">
            @error('achievements.'.$u.'.level')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Nama Prestasi</label>
            <input type="text" name="achievements[{{ $u }}][name]" value="@if(! empty($student->achievements[$u]->name)){{ $student->achievements[$u]->name }}@endif">
            @error('achievements.'.$u.'.name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Tahun</label>
            <select name="achievements[{{ $u }}][year]">
                <option value="">--</option>
                @for ($i=1970;$i<=2020;$i++) <option value="{{ $i }}" @if (! empty($student->achievements[$u]->year) && $student->achievements[$u]->year==$i) selected @endif>{{ $i }}</option>
                    @endfor
            </select>
            @error('achievements.'.$u.'.year')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-lg-12 mb-2">
            <label>Penyelenggara</label>
            <input type="text" name="achievements[{{ $u }}][organizer]" value="@if(! empty($student->achievements[$u]->organizer)){{ $student->achievements[$u]->organizer }}@endif">
            @error('achievements.'.$u.'.organizer')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    </div>
    @endfor