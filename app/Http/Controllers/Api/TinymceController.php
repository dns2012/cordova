<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ImgurService;
use Illuminate\Http\Request;

class TinymceController extends Controller
{
    public function upload(Request $request)
    {
        return [
            'location' => ImgurService::upload($request->file('file'))
        ];
    }
}
