@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Berita - Sekolah Alam Cordova</title>
<meta name="title" content="Berita - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Berita - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Berita - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Berita - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Berita</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.news') }}">berita kami</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Blogs -->
<section class="blog section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Berita Terbaru <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($newses as $news)
            <div class="col-lg-4">
                <!-- Single Blog -->
                <div class="single-blog">
                    <div class="blog-head overlay">
                        <div class="date">
                            <h4>{{ date('d', strtotime($news->created_at)) }}<span>{{ date('M', strtotime($news->created_at)) }}</span></h4>
                        </div>
                        <img src="{{ $news->image }}" alt="#">
                    </div>
                    <div class="blog-content">
                        <h4 class="blog-title" style="height: 60px"><a href="{{ route('front.news.detail', $news->slug) }}">{{ $news->title }}</a></h4>
                        <div class="blog-info">
                            <a href="#"><i class="fa fa-user"></i>By: Admin</a>
                            <a href="#"><i class="fa fa-heart-o"></i>{{ $news->pageview }} Views</a>
                        </div>
                        <p>{{ strip_tags(Str::words($news->description, 18)) }}</p>
                        <div class="button">
                            <a href="{{ route('front.news.detail', $news->slug) }}" class="btn">Lanjut Baca<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- End Single Blog -->
            </div>
            @endforeach
        </div>
        <div class="row block-nav justify-content-center mt-5">
            <div class="col-lg-12 col-7 block-nav__box">
                {{ $newses->links() }}
            </div>
        </div>
    </div>
</section>
<!--/ End Blogs -->
@endsection