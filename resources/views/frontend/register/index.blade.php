@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova</title>
<meta name="title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Pendaftaran Peserta Didik Baru - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Pendaftaran Peserta Didik Baru</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.pdb.step-1') }}">PDB</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Contact Us -->
<section id="contact" class="contact section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>Pendaftaran Peserta Didik Baru <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="contact-head">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bs-stepper">
                        <div class="bs-stepper-header" role="tablist">
                            <!-- your steps here -->
                            <div class="step @if($step==1) active @endif" data-target="#students-part" onclick="window.location='{{ route('front.pdb.step-1') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="students-part" id="students-part-trigger">
                                    <span class="bs-stepper-circle">1</span>
                                    <span class="bs-stepper-label">Identitas Peserta Didik</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div class="step @if($step==2) active @endif" data-target="#parents-part" onclick="window.location='{{ route('front.pdb.step-2') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="parents-part" id="parents-part-trigger">
                                    <span class="bs-stepper-circle">2</span>
                                    <span class="bs-stepper-label">Identitas Orang Tua</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div class="step @if($step==3) active @endif" data-target="#periodics-part" onclick="window.location='{{ route('front.pdb.step-3') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="periodics-part" id="periodics-part-trigger">
                                    <span class="bs-stepper-circle">3</span>
                                    <span class="bs-stepper-label">Informasi Periodik</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div class="step @if($step==4) active @endif" data-target="#achievements-part" onclick="window.location='{{ route('front.pdb.step-4') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="achievements-part" id="achievements-part-trigger">
                                    <span class="bs-stepper-circle">4</span>
                                    <span class="bs-stepper-label">Catatan Prestasi</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div class="step @if($step==5) active @endif" data-target="#scholarships-part" onclick="window.location='{{ route('front.pdb.step-5') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="scholarships-part" id="scholarships-part-trigger">
                                    <span class="bs-stepper-circle">5</span>
                                    <span class="bs-stepper-label">Beasiswa</span>
                                </button>
                            </div>
                            <div class="line"></div>
                            <div class="step @if($step==6) active @endif" data-target="#confirm-part" onclick="window.location='{{ route('front.pdb.step-6') }}'">
                                <button type="button" class="step-trigger" role="tab" aria-controls="confirm-part" id="confirm-part-trigger">
                                    <span class="bs-stepper-circle">6</span>
                                    <span class="bs-stepper-label">Konfirmasi</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-5 form-cordova">
                    <form action="{{ route('front.pdb.store', $step) }}" method="POST" class="form">
                        @csrf
                        @if ($step == 1)
                            @include('frontend.register.step_1')
                        @elseif ($step == 2)
                            @include('frontend.register.step_2')
                        @elseif ($step == 3)
                            @include('frontend.register.step_3')
                        @elseif ($step == 4)
                            @include('frontend.register.step_4')
                        @elseif ($step == 5)
                            @include('frontend.register.step_5')
                        @else
                            @include('frontend.register.step_6')
                        @endif
                        <div class="form-group">
                            <div class="row">
                                @if ($step == 6)
                                <div class="col-lg-12 text-center">
                                    <div class="mb-3">
                                        <input type="checkbox" name="confirm" value="1" required style="cursor: pointer">
                                        @error('confirm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <label for="confirm">Dengan ini saya menyatakan informasi peserta didik yang saya berikan benar apa adanya, dapat dipertanggung jawabkan sebagaimana mestinya dan saya setuju menyerahkan hak kelola informasi kepada Sekolah Alam Cordova.</label>
                                    </div>
                                    <button type="submit" class="btn">KONFIRMASI</button>
                                </div> 
                                @else
                                @if ($step != 1)
                                <div class="col-lg-6">
                                    <a href="{{ route('front.pdb.step-' . $previous_step) }}" class="btn">
                                        <i class="fa fa-chevron-left"></i> KEMBALI
                                    </a>
                                </div>
                                @endif
                                <div class="@if($step == 1) col-lg-12 @else col-lg-6 @endif text-right">
                                    <button type="submit" class="btn">
                                        LANJUTKAN <i class="fa fa-chevron-right"></i>
                                    </button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Contact Us -->
@endsection