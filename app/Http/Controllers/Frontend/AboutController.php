<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        return view('frontend.about.index', [
            'about' => About::findOrFail(1),
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
        ]);
    }
}
