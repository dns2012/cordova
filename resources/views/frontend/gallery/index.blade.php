@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Galeri - Sekolah Alam Cordova</title>
<meta name="title" content="Galeri - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Galeri - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Galeri - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Galeri - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Galeri</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.gallery') }}">galeri kami</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Events -->
<section class="events section">
    <div class="container">
        <div class="row">
            <div class="col-12 wow zoomIn">
                <div class="section-title">
                    <h2>Galeri {{ $stage }} <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($galleries as $gallery)
            <div class="col-lg-4">
                <!-- Single Event -->
                <div class="single-event">
                    <div class="head overlay">
                        <img src="{{ $gallery->image }}" alt="#">
                        <a href="{{ $gallery->image }}" data-fancybox="gallery" data-caption="* {{ $gallery->title }}" class="btn"><i class="fa fa-search"></i></a>
                    </div>
                </div>
                <!--/ End Single Event -->
            </div>
            @endforeach
        </div>
        <div class="row block-nav justify-content-center mt-5">
            <div class="col-lg-12 col-7 block-nav__box">
                {{ $galleries->links() }}
            </div>
        </div>
    </div>
</section>
<!--/ End Events -->
@endsection