@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>{{ $news->title }}</title>
<meta name="title" content="{{ $news->title }}">
<meta name="description" content="{{ Str::words($news->description, 80) }}">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="{{ $news->title }}">
<meta itemprop="description" content="{{ Str::words($news->description, 80) }}">
<meta itemprop="image" content="{{ $news->image }}">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{ $news->title }}">
<meta property="og:description" content="{{ Str::words($news->description, 80) }}">
<meta property="og:image" content="{{ $news->image }}">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{ $news->title }}">
<meta name="twitter:description" content="{{ Str::words($news->description, 80) }}">
<meta name="twitter:image" content="{{ $news->image }}">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Berita</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="{{ route('front.news') }}">berita kami<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.news.detail', $news->slug) }}">{{ $news->title }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses section-bg section">
    <div class="container">
        <div class="row">
            <div class="col-12 wow zoomIn">
                <div class="section-title">
                    <h2><span>{{ $news->title }}</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-lg-10">
                <div style="width: auto: height: auto" class="mb-4">
                    <img src="{{ $news->image }}" alt="{{ $news->slug }}" style="width: 100%; height: 100%; object-fit: cover">
                </div>

                {!! $news->description !!}
            </div>
        </div>
    </div>
</section>
<!--/ End Courses -->

@endsection