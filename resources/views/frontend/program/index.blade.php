@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Program - Sekolah Alam Cordova</title>
<meta name="title" content="Program - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Program - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Program - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Program - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>Program Kami</h2>
				<ul class="bread-list">
					<li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
					<li class="active"><a href="{{ route('front.program') }}">program kami</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses section-bg section">
	<div class="container">
		<div class="row">
			<div class="col-12 wow zoomIn">
				<div class="section-title">
					<h2>Program <span>Kami</span></h2>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			@foreach ($programs as $program)
			<div class="col-lg-4 mb-4">
				<!-- Single Course -->
				<div class="single-course">
					<div class="course-head overlay">
						<img src="{{ $program->image }}" alt="#">
						<a href="{{ route('front.program.detail', $program->slug) }}" class="btn"><i class="fa fa-link"></i></a>
					</div>
					<div class="single-content">
						<h4 style="height: 38px"><a href="{{ route('front.program.detail', $program->slug) }}">{{ $program->title }}</a></h4>
						<div style="height: 75px">
							<p>{{ strip_tags(Str::words($program->description, 10)) }}</p>
						</div>
					</div>
				</div>
				<!--/ End Single Course -->
			</div>
			@endforeach
		</div>
	</div>
</section>
<!--/ End Courses -->

@endsection