@extends('frontend.layouts.extend')

@section('meta')
<!-- HTML Meta Tags -->
<title>Kontak - Sekolah Alam Cordova</title>
<meta name="title" content="Kontak - Sekolah Alam Cordova">
<meta name="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="keywords" content="sekolah, alam, cordova">
<meta name="robots" content="index, follow, noodp">

<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Kontak - Sekolah Alam Cordova">
<meta itemprop="description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta itemprop="image" content="{{ asset('learnedu') }}/images/logo-cordova.png">

<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Kontak - Sekolah Alam Cordova">
<meta property="og:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta property="og:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Kontak - Sekolah Alam Cordova">
<meta name="twitter:description" content="Sekolah Alam Cordova adalah pendidikan alternatif yang menggunakan alam semesta sebagai tempat belajar, bahan mengajar dan sebagai objek pembelajaran">
<meta name="twitter:image" content="{{ asset('learnedu') }}/images/logo-cordova.png">
@endsection

@section('content')
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Kontak</h2>
                <ul class="bread-list">
                    <li><a href="{{ route('front.home') }}">Beranda<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="{{ route('front.contact') }}">kontak kami</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Contact Us -->
<section id="contact" class="contact section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>Kontak <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="contact-head">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="contact-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1976.5257236187874!2d113.2323511563684!3d-7.784370379400659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7ad18accd7433%3A0x88e7d3b060bdc314!2sSEKOLAH%20ALAM%20CORDOVA!5e0!3m2!1sen!2sid!4v1579702407098!5m2!1sen!2sid" width="600" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-head">
                        <!-- Form -->
                        <form class="form" action="#" id="contact-form">
                            <div class="form-group">
                                <input name="name" type="text" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="form-group">
                                <input name="phone" type="number" placeholder="No HP / WA" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" placeholder="Pertanyaan / Kritik & Saran" required></textarea>
                            </div>
                            <div class="form-group">
                                <div class="button">
                                    <button type="submit" class="btn primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <!--/ End Form -->
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-bottom">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-map"></i></div>
                        <h3>Lokasi</h3>
                        <p>Jl.Sunan Giri RT.02 RW.05 Mantong, Sumber Taman, Wonoasih , Kota Probolinggo</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-envelope"></i></div>
                        <h3>Alamat Email</h3>
                        <a href="mailto:information@gmail.com">support@salamcordova.sch.id</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <h3>Hubungi Kami</h3>
                        <p>0335 4494501 / 0822 3027 5227</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Contact Us -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            let name = $('[name=name]').val();
            let phone = $('[name=phone]').val();
            let message = $('[name=message]').val();
            let templateMessage =   window.encodeURIComponent (
                                'Hai, Admin Sekolah Cordova \n\n' +
                                'Nama Saya : ' + name + '\n\n' +
                                'HP : ' + phone + '\n\n' +
                                'Berikut pertanyaan / saran dari saya : \n\n' + message
                            );
            let url = 'https://web.whatsapp.com/send?phone=6285258795244&text=' + templateMessage;
            waSend(url, 'Salam Cordova', 1050, 550);
        })

        function waSend(url, title, width, height) {
            let left = (screen.width - width) / 2;
            let top = (screen.height - height) / 4;
            let myWindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
        }
    })
</script>
@endsection