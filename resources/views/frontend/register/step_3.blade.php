<div class="form-group">
    <label>Tinggi Badan (cm)</label>
    <input type="text" name="height" value="@if(! empty($student->height)){{ $student->height }}@endif" required>
    @error('height')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Berat Badan (kg)</label>
    <input type="text" name="weight" value="@if(! empty($student->weight)){{ $student->weight }}@endif" required>
    @error('weight')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Jarak ke Sekolah (km)</label>
    <input type="text" name="distance" value="@if(! empty($student->distance)){{ $student->distance }}@endif" required>
    @error('distance')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Waktu Tempuh ke Sekolah (menit)</label>
    <input type="text" name="distance_time" value="@if(! empty($student->distance_time)){{ $student->distance_time }}@endif" required>
    @error('distance_time')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label>Jumlah Saudara</label>
    <input type="text" name="brother" value="@if(! empty($student->brother)){{ $student->brother }}@endif" required>
    @error('brother')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>