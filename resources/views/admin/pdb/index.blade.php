@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">PPDB</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('news.index') }}">PPDB</a>
                            </li>
                            <li class="breadcrumb-item active">
                                @if (Route::current()->parameter('status') == 1)
                                Baru
                                @else
                                Proses
                                @endif
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-body">
                                <div class="form-group">
                                    <select class="form-control" name="stage">
                                        <option value="all" @if($stage == 'all') selected @endif>Semua</option>
                                        <option value="TK" @if($stage == 'TK') selected @endif>TK</option>
                                        <option value="SD" @if($stage == 'SD') selected @endif>SD</option>
                                        <option value="SMP" @if($stage == 'SMP') selected @endif>SMP</option>
                                    </select>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No Pendaftaran</th>
                                            <th>Jenjang</th>
                                            <th>Nama</th>
                                            <th>No HP</th>
                                            <th>Tanggal Daftar</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pdbs as $index => $pdb)
                                        @php $student = json_decode($pdb->student) @endphp
                                        <tr>
                                            <th scope="row">{{ $index + 1 }}</th>
                                            <td>{{ $pdb->visitor }}</td>
                                            <td>@if(! empty($student->stage)){{ $student->stage }}@else-@endif</td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->phone }}</td>
                                            <td>{{ $pdb->created_at }}</td>
                                            <td>
                                                @if ($pdb->status == 1)
                                                <div class="block-news__card-footer-grid">
                                                    <div class="block-news__card-footer-grid-item">
                                                        <button class="btn btn-success full-width" title="Detail" onclick="window.location='{{ route('pdb.detail', $pdb->id) }}'">
                                                            <i class="ft-sun"></i> LIHAT
                                                        </button>
                                                    </div>
                                                    <div class="block-news__card-footer-grid-item">
                                                        <form action="{{ route('pdb.process', $pdb->id) }}" method="POST" onsubmit="return confirm('Are you sure?')">
                                                            @csrf
                                                            @method('PUT')
                                                            <button class="btn btn-info full-width" title="Process">
                                                                <i class="ft-check-square"></i> PROSES
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="block-news__card-footer-grid-item">
                                                    <button class="btn btn-success full-width" title="Detail" onclick="window.location='{{ route('pdb.detail', $pdb->id) }}'">
                                                        <i class="ft-sun"></i> LIHAT
                                                    </button>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row block-nav justify-content-center">
                <div class="col-lg-12 col-7 block-nav__box">
                    {{ $pdbs->links() }}
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sticky no-padding" title="Add New" onclick="window.location='{{ route('news.create') }}'">
        <i class="ft-plus la-3x"></i>
    </button>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('[name=stage]').change(function() {
            window.location = $(this).val();
        })
    })
</script>
@endsection