<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\StoreRegister;
use App\Pdb;
use Illuminate\Http\Request;
use Validator;

class RegisterController extends Controller
{

    public function stepOne()
    {
        if (empty(session('visitor'))) {
            session([
                'visitor' => time()
            ]);
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'vehicle' => [
                'Jalan Kaki', 'Kendaraan Pribadi', 'Angkutan Umum', 'Mobil / Bus Jemput',
                'Kereta Api', 'Ojek', 'Andong', 'Mobil Pribadi', 'Lainnya'
            ],
            'live' => ['Bersama Orang Tua', 'Wali', 'Kost', 'Asrama', 'Panti Asuhan', 'Lainnya'],
            'step' => 1,
            'student' => $this->getStudent('decode')
        ]);
    }

    public function stepTwo()
    {
        $student = $this->getStudent('decode');

        if (empty(session('visitor')) || empty($student->name)) {
            return redirect()->route('front.pdb.step-1');
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'jobs' => [
                'Tidak Bekerja', 'Nelayan', 'Petani', 'Peternak', 'PNS', 'Karyawan Swasta',
                'Pedagang Besar', 'Pedagang Kecil', 'Wiraswasta', 'Wirausaha', 'Buruh',
                'Pensiunan'
            ],
            'educations' => [
                'Tidak Sekolah', 'Paud', 'TK / Sederajat', 'Putus SD', 'SD / Sederajat',
                'SMP / Sederajat', 'SMA / Sederajat', 'Paket A', 'Paket B', 'Paket C', 'D1',
                'D2', 'D3', 'S1', 'S2', 'S3', 'Non Formal', 'Informal', 'Lainnya'
            ],
            'salaries' => [
                '< Rp 500.000', 'Rp 500.000 - Rp 999.999', 'Rp 1.000.000 - Rp 1.999.999',
                'Rp 2.000.000 - Rp 4.999.999', 'Rp 5.000.000 - Rp 20.000.000', '> Rp 20.000.000'
            ],
            'previous_step' => 1,
            'step' => 2,
            'student' => $student
        ]);
    }

    public function stepThree()
    {
        $student = $this->getStudent('decode');

        if (empty(session('visitor')) || empty($student->father_name)) {
            return redirect()->route('front.pdb.step-2');
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'jobs' => [
                'Tidak Bekerja', 'Nelayan', 'Petani', 'Peternak', 'PNS', 'Karyawan Swasta',
                'Pedagang Besar', 'Pedagang Kecil', 'Wiraswasta', 'Wirausaha', 'Buruh',
                'Pensiunan'
            ],
            'educations' => [
                'Tidak Sekolah', 'Paud', 'TK / Sederajat', 'Putus SD', 'SD / Sederajat',
                'SMP / Sederajat', 'SMA / Sederajat', 'Paket A', 'Paket B', 'Paket C', 'D1',
                'D2', 'D3', 'S1', 'S2', 'S3', 'Non Formal', 'Informal', 'Lainnya'
            ],
            'salaries' => [
                '< Rp 500.000', 'Rp 500.000 - Rp 999.999', 'Rp 1.000.000 - Rp 1.999.999',
                'Rp 2.000.000 - Rp 4.999.999', 'Rp 5.000.000 - Rp 20.000.000', '> Rp 20.000.000'
            ],
            'previous_step' => 2,
            'step' => 3,
            'student' => $student
        ]);
    }

    public function stepFour()
    {
        $student = $this->getStudent('decode');

        if (empty(session('visitor')) || empty($student->height)) {
            return redirect()->route('front.pdb.step-3');
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'achievements' => ['one', 'two', 'three', 'four'],
            'previous_step' => 3,
            'step' => 4,
            'student' => $student
        ]);
    }

    public function stepFive()
    {
        $student = $this->getStudent('decode');

        if (empty(session('visitor')) || empty($student->height)) {
            return redirect()->route('front.pdb.step-1');
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'previous_step' => 4,
            'step' => 5,
            'student' => $student
        ]);
    }

    public function stepSix()
    {
        $student = $this->getStudent('decode');

        if (empty(session('visitor')) || empty($student->height)) {
            return redirect()->route('front.pdb.step-1');
        }

        return view('frontend.register.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'previous_step' => 5,
            'step' => 6,
            'student' => $student
        ]);
    }

    public function complete()
    {
        $student = $this->getStudent('decode');

        session()->flush();

        return view('frontend.register.complete', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'student' => $student
        ]);
    }

    public function store(StoreRegister $request, $id)
    {
        $step = [1, 2, 3, 4, 5, 6];

        if (!in_array($id, $step)) {
            abort(404);
        }

        $request->validated();

        $pdb = $this->getStudent(null, 'object');

        if (!$pdb) {
            $pdb = new Pdb;
        }

        $student = json_decode($pdb->student, TRUE);

        if ($id == 1) {
            $student['name'] = $request->input('name');
            $student['gender'] = $request->input('gender');
            $student['stage'] = $request->input('stage');
            $student['nisn'] = $request->input('nisn');
            $student['nik'] = $request->input('nik');
            $student['birth_place'] = $request->input('birth_place');
            $student['birth_date'] = $request->input('birth_date');
            $student['birth_month'] = $request->input('birth_month');
            $student['birth_year'] = $request->input('birth_year');
            $student['religion'] = $request->input('religion');
            $student['disability'] = $request->input('disability');
            $student['address'] = $request->input('address');
            $student['village'] = $request->input('village');
            $student['rt'] = $request->input('rt');
            $student['rw'] = $request->input('rw');
            $student['village_office'] = $request->input('village_office');
            $student['postal_code'] = $request->input('postal_code');
            $student['sub_district'] = $request->input('sub_district');
            $student['city'] = $request->input('city');
            $student['province'] = $request->input('province');
            $student['vehicle'] = $request->input('vehicle');
            $student['live'] = $request->input('live');
            $student['house_phone'] = $request->input('house_phone');
            $student['phone'] = $request->input('phone');
            $student['email'] = $request->input('email');
            $student['kps'] = $request->input('kps');
            $student['kps_number'] = $request->input('kps_number');
            $student['nationality'] = $request->input('nationality');
            $student['pp'] = $request->input('pp');
            $student['bank'] = $request->input('bank');
            $student['bank_account'] = $request->input('bank_account');
            $next_step = 2;
        } elseif ($id == 2) {
            $student['father_name'] = $request->input('father_name');
            $student['father_birth_year'] = $request->input('father_birth_year');
            $student['father_disability'] = $request->input('father_disability');
            $student['father_job'] = $request->input('father_job');
            $student['father_education'] = $request->input('father_education');
            $student['father_salary'] = $request->input('father_salary');
            $student['mother_name'] = $request->input('mother_name');
            $student['mother_birth_year'] = $request->input('mother_birth_year');
            $student['mother_disability'] = $request->input('mother_disability');
            $student['mother_job'] = $request->input('mother_job');
            $student['mother_education'] = $request->input('mother_education');
            $student['mother_salary'] = $request->input('mother_salary');
            $student['guardian_name'] = $request->input('guardian_name');
            $student['guardian_birth_year'] = $request->input('guardian_birth_year');
            $student['guardian_disability'] = $request->input('guardian_disability');
            $student['guardian_job'] = $request->input('guardian_job');
            $student['guardian_education'] = $request->input('guardian_education');
            $student['guardian_salary'] = $request->input('guardian_salary');
            $next_step = 3;
        } elseif ($id == 3) {
            $student['height'] = $request->input('height');
            $student['weight'] = $request->input('weight');
            $student['distance'] = $request->input('distance');
            $student['distance_time'] = $request->input('distance_time');
            $student['brother'] = $request->input('brother');
            $next_step = 4;
        } elseif ($id == 4) {
            $achievements = null;

            if (! empty($request->input('achievements'))) {
                foreach ($request->input('achievements') as $achievement) {
                    if (!empty($achievement['kind']) || !empty($achievement['name'])) {
                        $achievements[] = $achievement;
                    }
                }
            }

            $student['achievements'] = $achievements;
            $next_step = 5;
        } elseif ($id == 5) {
            $scholarships = null;

            if (! empty($request->input('scholarships'))) {
                foreach ($request->input('scholarships') as $scholarship) {
                    if (!empty($scholarship['kind']) || !empty($scholarship['organizer'])) {
                        $scholarships[] = $scholarship;
                    }
                }
            }
            
            $student['scholarships'] = $scholarships;
            $next_step = 6;
        } elseif ($id == 6) {
            $pdb->status = 1;
        }

        $pdb->student = json_encode($student);

        $pdb->save();

        if (!empty($pdb->status)) {
            return redirect()->route('front.pdb.complete');
        }

        return redirect()->route('front.pdb.step-' . $next_step);
    }

    public function getStudent($kind = null, $object = null, $session = null)
    {
        $student_data = Pdb::firstWhere('visitor', session('visitor'));

        if ($object) {
            return $student_data;
        }

        if (! $student_data) {
            try {
                $student_data = new Pdb;
                $student_data->visitor = session('visitor');
                $student_data->student = json_encode([]);
                $student_data->status = 0;
                $student_data->save();
            } catch (\Throwable $th) {
                abort(403, 'Page Expired');
            }
        }

        return ($kind == 'decode') ? json_decode($student_data->student) : $student_data->student;
    }
}
