<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Event;
use App\Http\Controllers\Controller;
use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function index()
    {
        return view('frontend.program.index', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'programs' => Program::orderBy('id', 'DESC')->paginate(9),
        ]);
    }

    public function detail($slug = null)
    {
        return view('frontend.program.detail', [
            'events' => Event::orderBy('id', 'DESC')->limit(3)->get(),
            'program' => Program::where('slug', $slug)->firstOrFail() 
        ]);
    }
}
