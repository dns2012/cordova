<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTeacher;
use App\Http\Requests\UpdateTeacher;
use App\Services\CloudinaryService;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.teacher.index', [
            'teachers' => Teacher::orderBy('id', 'DESC')->paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeacher $request)
    {
        $request->validated();

        $teacher = new Teacher;
        $teacher->name = $request->input('name');
        $teacher->motto = $request->input('motto');
        $teacher->position = $request->input('position');
        $teacher->avatar = CloudinaryService::upload($request->file('avatar'));
        $teacher->media = json_encode([
            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'linkedin' => $request->input('linkedin'),
            'instagram' => $request->input('instagram'),
        ]);
        $teacher->slug = Str::slug($teacher->name);

        if($teacher->save()) {
            return redirect()->route('teacher.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.teacher.edit', [
            'teacher' => Teacher::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacher $request, $id)
    {
        $request->validated();

        $teacher = Teacher::findOrFail($id);
        $teacher->name = $request->input('name');
        $teacher->motto = $request->input('motto');
        $teacher->position = $request->input('position');
        $teacher->media = json_encode([
            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'linkedin' => $request->input('linkedin'),
            'instagram' => $request->input('instagram'),
        ]);
        $teacher->slug = Str::slug($teacher->name);

        if ($request->file('avatar')) {
            CloudinaryService::remove($teacher->avatar);
            $teacher->avatar = CloudinaryService::upload($request->file('avatar'));
        }

        if($teacher->save()) {
            return redirect()->route('teacher.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::findOrFail($id);
        if($teacher->delete()) {
            CloudinaryService::remove($teacher->avatar);
            return redirect()->route('teacher.index');
        }
    }
}