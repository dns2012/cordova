@extends('admin.layouts.extend')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Teacher</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('teacher.index') }}">Teacher</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Edit
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row match-height justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-body">
                                <form method="POST" action="{{ route('teacher.update', $teacher->id) }}" enctype="multipart/form-data" class="forms-sample">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ $teacher->name }}" required>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text" name="position" class="form-control @error('position') is-invalid @enderror" placeholder="Position" value="{{ $teacher->position }}" required>
                                        @error('position')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Motto</label>
                                        <input type="text" name="motto" class="form-control @error('motto') is-invalid @enderror" placeholder="Motto" value="{{ $teacher->motto }}" required>
                                        @error('motto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Avatar <i style="font-size: 10px">(maks 600 x 700 250kb)</i></label>
                                        <br>
                                        <img src="{{ $teacher->avatar }}" class="img-fluid mb-1" alt="">
                                        <input type="file" name="avatar" class="form-control @error('avatar') is-invalid @enderror">
                                        @error('avatar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="row">
                                        @php $media = json_decode($teacher->media) @endphp
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Facebook Link</label>
                                                <input type="text" name="facebook" class="form-control @error('facebook') is-invalid @enderror" placeholder="Facebook" value="{{ $media->facebook }}">
                                                @error('facebook')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Twitter Link</label>
                                                <input type="text" name="twitter" class="form-control @error('twitter') is-invalid @enderror" placeholder="Twitter" value="{{ $media->twitter }}">
                                                @error('twitter')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>LinkedIn Link</label>
                                                <input type="text" name="linkedin" class="form-control @error('linkedin') is-invalid @enderror" placeholder="LinkedIn" value="{{ $media->linkedin }}">
                                                @error('linkedin')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Instagram Link</label>
                                                <input type="text" name="instagram" class="form-control @error('instagram') is-invalid @enderror" placeholder="Instagram" value="{{ $media->instagram }}">
                                                @error('instagram')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-rounded mr-2">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection